#!/usr/bin/fish

function abort
    echo SOMETHING BAD HAS HAPPENED
    exit 127
end

mkdir build 2>/dev/null

pushd src
rm -f ../build/puzzlemaker.love
zip -r ../build/puzzlemaker.love . || abort
popd

pushd build
if test ! -e love-11.4-win64
    wget2 -O love-11.4-win64.zip https://github.com/love2d/love/releases/download/11.4/love-11.4-win64.zip || abort
    unzip love-11.4-win64.zip || abort
end

rm -rf win64
mkdir win64
cp love-11.4-win64/*.dll win64/
cp ../readme.md win64/
cat love-11.4-win64/lovec.exe puzzlemaker.love > win64/puzzlemaker.exe
popd
