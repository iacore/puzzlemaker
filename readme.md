Löve2D paper puzzle maker - https://git.envs.net/iacore/puzzlemaker
Version 0.7.5+dev

This is a fork of [the original](https://www.hempuli.com/games/games.php?title=pp) made by Arvi Teikari (2020-2021).

[Controls](src/hint_controls.lua)

Other:
The buttons in top-right are for offsetting symbols on the cells.
The buttons on the right are for changing the grid type.
The button 'Rename' changes the title of the puzzle to be whatever text you have in your clipboard

For the strikethrough/wide strikethrough tools: the way the line points depends on which quadrant of a cell you click on. Sorry!

Inconsolata is an open-source font created by Raph Levien and released under the SIL Open Font License.

## Development

First, install `love` from [Löve2D](https://love2d.org/).

To run the application, run `love src` in terminal.
If you give an argument like `love src level.msgpack`, <kbd>S</kbd> and <kbd>L</kbd> will save/load to that file as messagepack.
