tools =
{
	A = {
		name = "Square marker",
		clear = true,
		layer = 2,
		icon = function(x,y,w,h)
			love.graphics.rectangle("fill", x + 3, y + 3, w - 6, h - 6)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	B = {
		name = "One number",
		clear = false,
		layer = 3,
		values = {"?"},
		icon = function(x,y,w,h,d,c,hover)
			local col = getnumcolour(hover,0,c[1])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[1]), x + w * 0.5 - currfontsize * 0.25 * numbermult * string.len(tostring(d[1])), y + h * 0.5 - currfontsize * 0.55 * numbermult, nil, numbermult, numbermult)
		end,
		func = function(d)
			currtool = d.code
		end,
		update = function(tile,value,slot)
			tile[slot+1] = value
			tile[slot+2] = currcolour
		end,
	},
	C = {
		name = "Two numbers",
		clear = false,
		layer = 3,
		values = {"?","?"},
		icon = function(x,y,w,h,d,c,hover)
			local col = getnumcolour(hover,0,c[1])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[1]), x + w * 0.5 - currfontsize * 0.25 * numbermult * string.len(tostring(d[1])) - w * 0.25, y + h * 0.5 - currfontsize * 0.55 * numbermult - h * 0.15, nil, numbermult, numbermult)

			col = getnumcolour(hover,1,c[2])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[2]), x + w * 0.5 - currfontsize * 0.25 * numbermult * string.len(tostring(d[2])) + w * 0.25, y + h * 0.5 - currfontsize * 0.55 * numbermult + h * 0.15, nil, numbermult, numbermult)
		end,
		func = function(d)
			currtool = d.code
		end,
		update = function(tile,value,slot)
			tile[slot+1] = value
			tile[slot+3] = currcolour
		end,
	},
	D = {
		name = "Three numbers",
		clear = false,
		layer = 3,
		values = {"?","?","?"},
		icon = function(x,y,w,h,d,c,hover)
			local nmult = numbermult * 0.75

			local col = getnumcolour(hover,0,c[1])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[1]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[1])) - w * 0.25, y + h * 0.5 - currfontsize * 0.55 * nmult - h * 0.2, nil, nmult, nmult)

			col = getnumcolour(hover,1,c[2])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[2]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[2])) + w * 0.25, y + h * 0.5 - currfontsize * 0.55 * nmult - h * 0.2, nil, nmult, nmult)

			col = getnumcolour(hover,2,c[3])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[3]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[3])), y + h * 0.5 - currfontsize * 0.55 * nmult + h * 0.2, nil, nmult, nmult)
		end,
		func = function(d)
			currtool = d.code
		end,
		update = function(tile,value,slot)
			tile[slot+1] = value
			tile[slot+4] = currcolour
		end,
	},
	E = {
		name = "Four numbers",
		clear = false,
		layer = 3,
		values = {"?","?","?","?"},
		icon = function(x,y,w,h,d,c,hover)
			local nmult = numbermult * 0.75

			local col = getnumcolour(hover,0,c[1])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[1]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[1])), y + h * 0.5 - currfontsize * 0.55 * nmult - h * 0.25, nil, nmult, nmult)

			col = getnumcolour(hover,1,c[2])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[2]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[2])) + w * 0.3, y + h * 0.5 - currfontsize * 0.55 * nmult, nil, nmult, nmult)

			col = getnumcolour(hover,2,c[3])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[3]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[3])), y + h * 0.5 - currfontsize * 0.55 * nmult + h * 0.25, nil, nmult, nmult)

			col = getnumcolour(hover,3,c[4])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[4]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[4])) - w * 0.3, y + h * 0.5 - currfontsize * 0.55 * nmult, nil, nmult, nmult)
		end,
		func = function(d)
			currtool = d.code
		end,
		update = function(tile,value,slot)
			tile[slot+1] = value
			tile[slot+5] = currcolour
		end,
	},
	F = {
		name = "Hollow circle",
		clear = false,
		layer = 4,
		icon = function(x,y,w,h)
			love.graphics.setLineWidth(2)
			love.graphics.circle("line", x + w * 0.5, y + w * 0.5, w * 0.5 - 6)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	G = {
		name = "Error marker",
		clear = true,
		layer = 1,
		icon = function(x,y,w,h)
			love.graphics.setColor(1.0, 0.2, 0.2, 0.5)
			love.graphics.rectangle("fill", x, y, w, h)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	H = {
		name = "Straight line",
		clear = true,
		layer = 2,
		values = {"0","0","0","0"},
		customcolours = true,
		icon = function(x,y,w,h,d)
			love.graphics.setColor(0, 0.5, 0, 1)

			if (d[1] == "0") and (d[2] == "0") and (d[3] == "0") and (d[4] == "0") then
				love.graphics.line(x + w * 0.5, y + h * 0.5, x + w * 0.5, y)
				love.graphics.line(x + w * 0.5, y + h * 0.5, x + w, y + h * 0.5)
			else
				if (d[1] ~= "0") then
					local c = getcolour(d[1])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x + w, y + h * 0.5)
				end

				if (d[2] ~= "0") then
					local c = getcolour(d[2])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x + w * 0.5, y)
				end

				if (d[3] ~= "0") then
					local c = getcolour(d[3])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x, y + h * 0.5)
				end

				if (d[4] ~= "0") then
					local c = getcolour(d[4])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x + w * 0.5, y + h)
				end
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy)
			if (x > hx) then
				return 2,hx,hy
			elseif (y < hy) then
				return 3,hx,hy
			elseif (x < hx) then
				return 4,hx,hy
			elseif (y > hy) then
				return 5,hx,hy
			else
				return nil,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("H",ox,oy,z,false,true)
			setsubvalue(currcolour,val,ox,oy,z)

			local rot = (((val - 2) + 2) % 4) + 2

			if inbounds(nx,ny) then
				placetile("H",nx,ny,z,false,true)
				setsubvalue(currcolour,rot,nx,ny,z)
			end
		end,
	},
	I = {
		name = "Dot marker",
		clear = true,
		layer = 2,
		icon = function(x,y,w,h)
			love.graphics.circle("fill", x + w * 0.5, y + w * 0.5, 6)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	J = {
		name = "Arrow",
		clear = false,
		layer = 4,
		values = {"0"},
		icon = function(x,y,w,h,d,c)
			local colour = getcolour(c[1])
			love.graphics.setColor(colour[1], colour[2], colour[3], 0.3)
			local p1x,p1y = 0,0
			local p2x,p2y = 0,0
			local p3x,p3y = 0,0

			if (d[1] == "0") then
				p1x = x
				p1y = y
				p2x = x + w
				p2y = y + h * 0.5
				p3x = x
				p3y = y + h
			elseif (d[1] == "1") then
				p1x = x
				p1y = y + h
				p2x = x + w * 0.5
				p2y = y
				p3x = x + w
				p3y = y + h
			elseif (d[1] == "2") then
				p1x = x
				p1y = y + h * 0.5
				p2x = x + w
				p2y = y
				p3x = x + w
				p3y = y + h
			elseif (d[1] == "3") then
				p1x = x
				p1y = y
				p2x = x + w
				p2y = y
				p3x = x + w * 0.5
				p3y = y + h
			end

			love.graphics.polygon("fill", {p1x, p1y, p2x, p2y, p3x, p3y})
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy)
			if (x > hx) then
				return "0",hx,hy
			elseif (y < hy) then
				return "1",hx,hy
			elseif (x < hx) then
				return "2",hx,hy
			elseif (y > hy) then
				return "3",hx,hy
			else
				return nil,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("J",ox,oy,z,false)
			setsubvalue(val,2,ox,oy,z)
			setsubvalue(currcolour,3,ox,oy,z)
		end,
	},
	K = {
		name = "Transparent square",
		clear = false,
		layer = 2,
		icon = function(x,y,w,h,d,c)
			local colour = getcolour(c[1])
			love.graphics.setColor(colour[1], colour[2], colour[3], 0.4)
			love.graphics.rectangle("fill", x, y, w, h)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	L = {
		name = "Square",
		clear = false,
		layer = 2,
		icon = function(x,y,w,h)
			love.graphics.rectangle("fill", x, y, w, h)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	M = {
		name = "Thick circle",
		clear = false,
		layer = 4,
		icon = function(x,y,w,h)
			love.graphics.circle("line", x + w * 0.5, y + w * 0.5, w * 0.33)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	N = {
		name = "Cross",
		clear = false,
		layer = 4,
		icon = function(x,y,w,h)
			love.graphics.line(x + 8, y + 8, x + w - 8, y + h - 8)
			love.graphics.line(x + 8, y + h - 8, x + w - 8, y + 8)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	O = {
		name = "Filled circle",
		clear = false,
		layer = 6,
		icon = function(x,y,w,h)
			love.graphics.setLineWidth(3)
			love.graphics.circle("fill", x + w * 0.5, y + w * 0.5, w * 0.36)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	P = {
		name = "Star",
		clear = false,
		layer = 4,
		icon = function(x,y,w,h)
			local p0x,p0y = x + w * 0.5, y + h * 0.08
			local p1x,p1y = x + w * 0.62, y + h * 0.38
			local p2x,p2y = x + w * 0.92, y + h * 0.45
			local p3x,p3y = x + w * 0.62, y + h * 0.58
			local p4x,p4y = x + w * 0.75, y + h * 0.92
			local p5x,p5y = x + w * 0.5, y + h * 0.66
			local p6x,p6y = x + w * 0.25, y + h * 0.92
			local p7x,p7y = x + w * 0.38, y + h * 0.58
			local p8x,p8y = x + w * 0.08, y + h * 0.45
			local p9x,p9y = x + w * 0.38, y + h * 0.38

			love.graphics.polygon("fill", {p9x, p9y, p0x, p0y, p1x, p1y})
			love.graphics.polygon("fill", {p1x, p1y, p2x, p2y, p3x, p3y})
			love.graphics.polygon("fill", {p7x, p7y, p4x, p4y, p1x, p1y})
			love.graphics.polygon("fill", {p3x, p3y, p6x, p6y, p9x, p9y})
			love.graphics.polygon("fill", {p7x, p7y, p8x, p8y, p9x, p9y})
			love.graphics.rectangle("fill", p9x, p9y, p3x - p9x, p3y - p9y)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	Q = {
		name = "Side arrow",
		clear = false,
		layer = 4,
		values = {"0"},
		icon = function(x,y,w,h,d,c)
			love.graphics.setLineWidth(2)
			local colour = getcolour(c[1])
			love.graphics.setColor(colour[1], colour[2], colour[3], 1)

			if (d[1] == "0") then
				love.graphics.line(x + 8, y + h - 8, x + w - 8, y + h - 8)
				love.graphics.line(x + w - 8, y + h - 8, x + w - 16, y + h - 12)
				love.graphics.line(x + w - 8, y + h - 8, x + w - 16, y + h - 4)
			elseif (d[1] == "1") then
				love.graphics.line(x + w - 8, y + 8, x + w - 8, y + h - 8)
				love.graphics.line(x + w - 8, y + 8, x + w - 12, y + 16)
				love.graphics.line(x + w - 8, y + 8, x + w - 4, y + 16)
			elseif (d[1] == "2") then
				love.graphics.line(x + 8, y + 8, x + w - 8, y + 8)
				love.graphics.line(x + 8, y + 8, x + 16, y + 12)
				love.graphics.line(x + 8, y + 8, x + 16, y + 4)
			elseif (d[1] == "3") then
				love.graphics.line(x + 8, y + 8, x + 8, y + h - 8)
				love.graphics.line(x + 8, y + h - 8, x + 12, y + h - 16)
				love.graphics.line(x + 8, y + h - 8, x + 4, y + h - 16)
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy)
			if (x > hx) then
				return "0",hx,hy
			elseif (y < hy) then
				return "1",hx,hy
			elseif (x < hx) then
				return "2",hx,hy
			elseif (y > hy) then
				return "3",hx,hy
			else
				return nil,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("Q",ox,oy,z,false)
			setsubvalue(val,2,ox,oy,z)
			setsubvalue(currcolour,3,ox,oy,z)
		end,
	},
	R = {
		name = "Strikethrough (janky)",
		clear = true,
		layer = 4,
		values = {"0","0"},
		customcolours = true,
		icon = function(x,y,w,h,d,c)
			if (d[1] == "0") and (d[2] == "0") then
				love.graphics.setColor(0,0,0,1)
				love.graphics.line(x, y, x + w, y + h)
			end

			if (d[1] ~= "0") then
				local colour = getcolour(d[1])
				love.graphics.setColor(colour[1], colour[2], colour[3], 1)
				love.graphics.line(x, y, x + w, y + h)
			end

			if (d[2] ~= "0") then
				local colour = getcolour(d[2])
				love.graphics.setColor(colour[1], colour[2], colour[3], 1)
				love.graphics.line(x, y + h, x + w, y)
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy,ax,ay,ahx,ahy)
			if (ax > ahx) and (ay > ahy) then
				return 2,hx,hy
			elseif (ax > ahx) and (ay < ahy) then
				return 3,hx,hy
			elseif (ax < ahx) and (ay < ahy) then
				return 2,hx,hy
			elseif (ax < ahx) and (ay > ahy) then
				return 3,hx,hy
			else
				return nil,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("R",ox,oy,z,false,true)
			setsubvalue(currcolour,val,ox,oy,z)
		end,
	},
	S = {
		name = "Edgeline",
		clear = false,
		layer = 5,
		values = {"0","0","0","0"},
		customcolours = true,
		icon = function(x,y,w,h,d)
			love.graphics.setLineWidth(3)
			love.graphics.setColor(0, 0.5, 0, 1)

			if (d[1] == "0") and (d[2] == "0") and (d[3] == "0") and (d[4] == "0") then
				love.graphics.line(x + w * 0.5, y, x + w * 0.5, y + h)
			else
				if (d[1] ~= "0") then
					local c = getcolour(d[1])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w, y, x + w, y + h)
				end

				if (d[2] ~= "0") then
					local c = getcolour(d[2])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x, y, x + w, y)
				end

				if (d[3] ~= "0") then
					local c = getcolour(d[3])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x, y, x, y + h)
				end

				if (d[4] ~= "0") then
					local c = getcolour(d[4])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x, y + h, x + w, y + h)
				end
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy)
			if (x > hx) then
				return 2,hx,hy
			elseif (y < hy) then
				return 3,hx,hy
			elseif (x < hx) then
				return 4,hx,hy
			elseif (y > hy) then
				return 5,hx,hy
			else
				return nil,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("S",ox,oy,z,false,true)
			local offsets = {{},{1,0,4},{0,-1,5},{-1,0,2},{0,1,3}}
			local d = offsets[val]
			local x,y = d[1],d[2]
			setsubvalue(currcolour,val,ox,oy,z)

			if inbounds(ox+x,oy+y) then
				placetile("S",ox+x,oy+y,z,false,true)
				setsubvalue(currcolour,d[3],ox+x,oy+y,z)
			end
		end,
	},
	T = {
		name = "Wide strikethrough (very janky)",
		clear = true,
		layer = 4,
		values = {"0","0","0","0"},
		customcolours = true,
		icon = function(x,y,w,h,d,c)
			if (d[1] == "0") and (d[2] == "0") and (d[3] == "0") and (d[4] == "0") then
				love.graphics.setColor(0,0,0,1)
				love.graphics.line(x, y, x + w, y + h * 0.5)
			end

			if (d[1] ~= "0") then
				local colour = getcolour(d[1])
				love.graphics.setColor(colour[1], colour[2], colour[3], 1)
				love.graphics.line(x + w, y + h, x, y - h)
			end

			if (d[2] ~= "0") then
				local colour = getcolour(d[2])
				love.graphics.setColor(colour[1], colour[2], colour[3], 1)
				love.graphics.line(x + w, y, x, y + h * 2)
			end

			if (d[3] ~= "0") then
				local colour = getcolour(d[3])
				love.graphics.setColor(colour[1], colour[2], colour[3], 1)
				love.graphics.line(x, y, x + w * 2, y + h)
			end

			if (d[4] ~= "0") then
				local colour = getcolour(d[4])
				love.graphics.setColor(colour[1], colour[2], colour[3], 1)
				love.graphics.line(x, y + h, x + w * 2, y)
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy,ax,ay,ahx,ahy)
			if (ax > ahx) and (ay > ahy) then
				return 2,hx,hy
			elseif (ax > ahx) and (ay < ahy) then
				return 3,hx,hy
			elseif (ax < ahx) and (ay < ahy) then
				return 4,hx,hy
			elseif (ax < ahx) and (ay > ahy) then
				return 5,hx,hy
			else
				return nil,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("T",ox,oy,z,false,true)
			setsubvalue(currcolour,val,ox,oy,z)
		end,
	},
	U = {
		name = "Dotted edgeline",
		clear = false,
		layer = 6,
		values = {"0","0","0","0"},
		customcolours = true,
		icon = function(x,y,w,h,d)
			love.graphics.setLineWidth(2)
			love.graphics.setColor(0, 0.5, 0, 1)

			if (d[1] == "0") and (d[2] == "0") and (d[3] == "0") and (d[4] == "0") then
				love.graphics.circle("fill", x + w * 0.5, y + h * 0.0, 3)
				love.graphics.circle("fill", x + w * 0.5, y + h * 0.25, 3)
				love.graphics.circle("fill", x + w * 0.5, y + h * 0.5, 3)
				love.graphics.circle("fill", x + w * 0.5, y + h * 0.75, 3)
				love.graphics.circle("fill", x + w * 0.5, y + h * 1.0, 3)
			else
				if (d[1] ~= "0") then
					local c = getcolour(d[1])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.circle("fill", x + w, y + h * 0.0, 3)
					love.graphics.circle("fill", x + w, y + h * 0.25, 3)
					love.graphics.circle("fill", x + w, y + h * 0.5, 3)
					love.graphics.circle("fill", x + w, y + h * 0.75, 3)
					love.graphics.circle("fill", x + w, y + h * 1.0, 3)
				end

				if (d[2] ~= "0") then
					local c = getcolour(d[2])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.circle("fill", x + w * 0.0, y, 3)
					love.graphics.circle("fill", x + w * 0.25, y, 3)
					love.graphics.circle("fill", x + w * 0.5, y, 3)
					love.graphics.circle("fill", x + w * 0.75, y, 3)
					love.graphics.circle("fill", x + w * 1.0, y, 3)
				end

				if (d[3] ~= "0") then
					local c = getcolour(d[3])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.circle("fill", x, y + h * 0.0, 3)
					love.graphics.circle("fill", x, y + h * 0.25, 3)
					love.graphics.circle("fill", x, y + h * 0.5, 3)
					love.graphics.circle("fill", x, y + h * 0.75, 3)
					love.graphics.circle("fill", x, y + h * 1.0, 3)
				end

				if (d[4] ~= "0") then
					local c = getcolour(d[4])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.circle("fill", x + w * 0.0, y + h, 3)
					love.graphics.circle("fill", x + w * 0.25, y + h, 3)
					love.graphics.circle("fill", x + w * 0.5, y + h, 3)
					love.graphics.circle("fill", x + w * 0.75, y + h, 3)
					love.graphics.circle("fill", x + w * 1.0, y + h, 3)
				end
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy)
			if (x > hx) then
				return 2,hx,hy
			elseif (y < hy) then
				return 3,hx,hy
			elseif (x < hx) then
				return 4,hx,hy
			elseif (y > hy) then
				return 5,hx,hy
			else
				return nil,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("U",ox,oy,z,false,true)
			local offsets = {{},{1,0,4},{0,-1,5},{-1,0,2},{0,1,3}}
			local d = offsets[val]
			local x,y = d[1],d[2]

			setsubvalue(currcolour,val,ox,oy,z)

			if inbounds(ox+x,oy+y) then
				placetile("U",ox+x,oy+y,z,false,true)
				setsubvalue(currcolour,d[3],ox+x,oy+y,z)
			end
		end,
	},
	V = {
		name = "Hollow square",
		clear = false,
		layer = 1,
		icon = function(x,y,w,h)
			love.graphics.rectangle("line", x + 6, y + 6, w - 12, h - 12)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	W = {
		name = "Two numbers with circle",
		clear = false,
		layer = 3,
		values = {"?","?"},
		icon = function(x,y,w,h,d,c,hover)
			local nmult = numbermult * 0.75

			local col = getnumcolour(hover,0,c[1])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[1]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[1])) - w * 0.18, y + h * 0.5 - currfontsize * 0.55 * nmult - h * 0.17, nil, nmult, nmult)

			love.graphics.setLineWidth(1)
			love.graphics.circle("line", x + w * 0.32, y + w * 0.35, w * 0.25)

			col = getnumcolour(hover,1,c[2])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[2]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[2])) + w * 0.25, y + h * 0.5 - currfontsize * 0.55 * nmult + h * 0.2, nil, nmult, nmult)
		end,
		func = function(d)
			currtool = d.code
		end,
		update = function(tile,value,slot)
			tile[slot+1] = value
			tile[slot+3] = currcolour
		end,
	},
	X = {
		name = "Freehand line (janky)",
		clear = true,
		layer = 2,
		values = {"0","0","0","0","0","0","0","0"},
		customcolours = true,
		icon = function(x,y,w,h,d)
			love.graphics.setColor(0, 0.5, 0, 1)

			if (d[1] == "0") and (d[2] == "0") and (d[3] == "0") and (d[4] == "0") and (d[5] == "0") and (d[6] == "0") and (d[7] == "0") and (d[8] == "0") then
				love.graphics.line(x + w * 0.5, y + h * 0.5, x + w, y + h * 0.5)
				love.graphics.line(x + w * 0.5, y + h * 0.5, x, y)
			else
				if (d[1] ~= "0") then
					local c = getcolour(d[1])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x + w, y + h * 0.5)
				end

				if (d[2] ~= "0") then
					local c = getcolour(d[2])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x + w * 0.5, y)
				end

				if (d[3] ~= "0") then
					local c = getcolour(d[3])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x, y + h * 0.5)
				end

				if (d[4] ~= "0") then
					local c = getcolour(d[4])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x + w * 0.5, y + h)
				end

				if (d[5] ~= "0") then
					local c = getcolour(d[5])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x + w, y + h)
				end

				if (d[6] ~= "0") then
					local c = getcolour(d[6])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x + w, y)
				end

				if (d[7] ~= "0") then
					local c = getcolour(d[7])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x, y)
				end

				if (d[8] ~= "0") then
					local c = getcolour(d[8])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x, y + h)
				end
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy,ax,ay,ahx,ahy)
			local cutoff = 0.4

			if (ax > ahx + tilesize * cutoff) and (ay > ahy + tilesize * cutoff) and (x == hx) and (y == hy) then
				return 8,hx,hy,hx+1,hy+1
			elseif (ax > ahx + tilesize * cutoff) and (ay < ahy - tilesize * cutoff) and (x == hx) and (y == hy) then
				return 9,hx,hy,hx+1,hy-1
			elseif (ax < ahx - tilesize * cutoff) and (ay < ahy - tilesize * cutoff) and (x == hx) and (y == hy) then
				return 6,hx,hy,hx-1,hy-1
			elseif (ax < ahx - tilesize * cutoff) and (ay > ahy + tilesize * cutoff) and (x == hx) and (y == hy) then
				return 7,hx,hy,hx-1,hy+1
			elseif (ax > ahx + tilesize * cutoff) and (ay > ahy + tilesize * cutoff) and (x > hx) and (y > hy) then
				return 6,hx,hy
			elseif (ax > ahx + tilesize * cutoff) and (ay < ahy - tilesize * cutoff) and (x > hx) and (y < hy) then
				return 7,hx,hy
			elseif (ax < ahx - tilesize * cutoff) and (ay < ahy - tilesize * cutoff) and (x < hx) and (y < hy) then
				return 8,hx,hy
			elseif (ax < ahx - tilesize * cutoff) and (ay > ahy + tilesize * cutoff) and (x < hx) and (y > hy) then
				return 9,hx,hy
			elseif (x > hx) and (math.abs(ay - ahy) < tilesize * cutoff) then
				return 2,hx,hy
			elseif (y < hy) and (math.abs(ax - ahx) < tilesize * cutoff) then
				return 3,hx,hy
			elseif (x < hx) and (math.abs(ay - ahy) < tilesize * cutoff) then
				return 4,hx,hy
			elseif (y > hy) and (math.abs(ax - ahx) < tilesize * cutoff) then
				return 5,hx,hy
			else
				return nil,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("X",ox,oy,z,false,true)
			setsubvalue(currcolour,val,ox,oy,z)

			local offset = math.floor((val-2)/4)
			local val_ = val - offset * 4

			local rot = (((val_ - 2) + 2) % 4) + 2 + offset * 4

			if inbounds(nx,ny) then
				placetile("X",nx,ny,z,false,true)
				setsubvalue(currcolour,rot,nx,ny,z)
			end
		end,
	},
	Y = {
		name = "Edge cross",
		clear = true,
		layer = 1,
		values = {"0","0","0","0"},
		customcolours = true,
		icon = function(x,y,w,h,d)
			love.graphics.setColor(0.8, 0, 0, 0.8)

			if (d[1] == "0") and (d[2] == "0") and (d[3] == "0") and (d[4] == "0") then
				love.graphics.line(x + w * 0.5 - 5, y + h * 0.5 - 5, x + w * 0.5 + 5, y + h * 0.5 + 5)
				love.graphics.line(x + w * 0.5 - 5, y + h * 0.5 + 5, x + w * 0.5 + 5, y + h * 0.5 - 5)
			else
				if (d[1] ~= "0") then
					local c = getcolour(d[1])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w - 5, y + h * 0.5 - 5, x + w + 5, y + h * 0.5 + 5)
					love.graphics.line(x + w - 5, y + h * 0.5 + 5, x + w + 5, y + h * 0.5 - 5)
				end

				if (d[2] ~= "0") then
					local c = getcolour(d[2])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5 - 5, y - 5, x + w * 0.5 + 5, y + 5)
					love.graphics.line(x + w * 0.5 - 5, y + 5, x + w * 0.5 + 5, y - 5)
				end

				if (d[3] ~= "0") then
					local c = getcolour(d[3])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x - 5, y + h * 0.5 - 5, x + 5, y + h * 0.5 + 5)
					love.graphics.line(x - 5, y + h * 0.5 + 5, x + 5, y + h * 0.5 - 5)
				end

				if (d[4] ~= "0") then
					local c = getcolour(d[4])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5 - 5, y + h - 5, x + w * 0.5 + 5, y + h + 5)
					love.graphics.line(x + w * 0.5 - 5, y + h + 5, x + w * 0.5 + 5, y + h - 5)
				end
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy)
			if (x > hx) then
				return 2,hx,hy
			elseif (y < hy) then
				return 3,hx,hy
			elseif (x < hx) then
				return 4,hx,hy
			elseif (y > hy) then
				return 5,hx,hy
			else
				return nil,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("Y",ox,oy,z,false,true)
			setsubvalue(currcolour,val,ox,oy,z)
		end,
	},
	Z = {
		name = "Flag",
		clear = false,
		layer = 3,
		values = {"?"},
		icon = function(x,y,w,h,d,c,hover)
			local col = getnumcolour(hover,0,c[1])
			local nmult = numbermult * 0.55
			love.graphics.setLineWidth(1)
			love.graphics.setColor(1, 1, 1, 1)
			love.graphics.rectangle("fill", x + w * 0.25, y + h * 0.2, w * 0.5, h * 0.4)

			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.rectangle("line", x + w * 0.25, y + h * 0.2, w * 0.5, h * 0.4)
			love.graphics.line(x + w * 0.25, y + h - 6, x + w * 0.25, y + h * 0.6)
			love.graphics.print(tostring(d[1]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[1])), y + h * 0.4 - currfontsize * 0.55 * nmult, nil, nmult, nmult)
		end,
		func = function(d)
			currtool = d.code
		end,
		update = function(tile,value,slot)
			tile[slot+1] = value
			tile[slot+2] = currcolour
		end,
	},
	["0"] = {
		name = "Plus",
		clear = false,
		layer = 4,
		icon = function(x,y,w,h)
			love.graphics.line(x + w * 0.5 - w * 0.25, y + h * 0.5, x + w * 0.5 + w * 0.25, y + h * 0.5)
			love.graphics.line(x + w * 0.5, y + h * 0.5 - h * 0.25, x + w * 0.5, y + h * 0.5 + h * 0.25)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	["1"] = {
		name = "Diagonal line",
		clear = true,
		layer = 2,
		values = {"0","0","0","0"},
		customcolours = true,
		icon = function(x,y,w,h,d)
			love.graphics.setColor(0, 0.5, 0, 1)

			if (d[1] == "0") and (d[2] == "0") and (d[3] == "0") and (d[4] == "0") then
				love.graphics.line(x + w * 0.5, y + h * 0.5, x + w, y)
				love.graphics.line(x + w * 0.5, y + h * 0.5, x, y)
			else
				if (d[1] ~= "0") then
					local c = getcolour(d[1])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x + w, y + h)
				end

				if (d[2] ~= "0") then
					local c = getcolour(d[2])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x + w, y)
				end

				if (d[3] ~= "0") then
					local c = getcolour(d[3])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x, y)
				end

				if (d[4] ~= "0") then
					local c = getcolour(d[4])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y + h * 0.5, x, y + h)
				end
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy,ax,ay,ahx,ahy)
			local cutoff = 0.4

			if (x > hx) and (y > hy) then
				return 2,hx,hy
			elseif (x > hx) and (y < hy) then
				return 3,hx,hy
			elseif (x < hx) and (y < hy) then
				return 4,hx,hy
			elseif (x < hx) and (y > hy) then
				return 5,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("1",ox,oy,z,false,true)
			setsubvalue(currcolour,val,ox,oy,z)

			local rot = (((val - 2) + 2) % 4) + 2

			if inbounds(nx,ny) then
				placetile("1",nx,ny,z,false,true)
				setsubvalue(currcolour,rot,nx,ny,z)
			end
		end,
	},
	["2"] = {
		name = "Corner triangle",
		clear = false,
		layer = 1,
		values = {"0","0","0","0"},
		customcolours = true,
		icon = function(x,y,w,h,d)
			love.graphics.setColor(0, 0, 0, 1)

			local x1,x2,x3,y1,y2,y3 = x,x,x,y,y,y

			if (d[1] == "0") and (d[2] == "0") and (d[3] == "0") and (d[4] == "0") then
				x1 = x
				y1 = y

				x2 = x + w
				y2 = y

				x3 = x
				y3 = y + h

				love.graphics.polygon("fill", {x1, y1, x2, y2, x3, y3})
			else
				if (d[1] ~= "0") then
					local c = getcolour(d[1])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					x1 = x + w
					y1 = y

					x2 = x + w
					y2 = y + h

					x3 = x
					y3 = y + h

					love.graphics.polygon("fill", {x1, y1, x2, y2, x3, y3})
				end

				if (d[2] ~= "0") then
					local c = getcolour(d[2])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					x1 = x
					y1 = y

					x2 = x + w
					y2 = y

					x3 = x + w
					y3 = y + h

					love.graphics.polygon("fill", {x1, y1, x2, y2, x3, y3})
				end

				if (d[3] ~= "0") then
					local c = getcolour(d[3])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					x1 = x
					y1 = y

					x2 = x + w
					y2 = y

					x3 = x
					y3 = y + h

					love.graphics.polygon("fill", {x1, y1, x2, y2, x3, y3})
				end

				if (d[4] ~= "0") then
					local c = getcolour(d[4])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					x1 = x
					y1 = y

					x2 = x + w
					y2 = y + h

					x3 = x
					y3 = y + h

					love.graphics.polygon("fill", {x1, y1, x2, y2, x3, y3})
				end
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy,ax,ay,ahx,ahy)
			local cutoff = 0.4

			if (x > hx) and (y > hy) then
				return 2,hx,hy
			elseif (x > hx) and (y < hy) then
				return 3,hx,hy
			elseif (x < hx) and (y < hy) then
				return 4,hx,hy
			elseif (x < hx) and (y > hy) then
				return 5,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("2",ox,oy,z,false,true)
			setsubvalue(currcolour,val,ox,oy,z)
		end,
	},
	["3"] = {
		name = "Circle with border",
		clear = false,
		layer = 7,
		icon_ui = function(x,y,w,h)
			love.graphics.setLineWidth(1)

			love.graphics.setColor(1, 1, 1, 1)
			love.graphics.circle("fill", x + w * 0.5, y + w * 0.5, w * 0.2)

			love.graphics.setColor(0, 0, 0, 1)
			love.graphics.circle("line", x + w * 0.5, y + w * 0.5, w * 0.2)
		end,
		icon = function(x,y,w,h,d,c)
			love.graphics.setLineWidth(1)

			local col = getcolour(c[1])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.circle("fill", x + w * 0.5, y + w * 0.5, w * 0.2)

			love.graphics.setColor(0, 0, 0, 1)
			love.graphics.circle("line", x + w * 0.5, y + w * 0.5, w * 0.2)
		end,
		func = function(d)
			currtool = d.code
		end,
	},
	["4"] = {
		name = "Small number",
		clear = false,
		layer = 3,
		values = {"?"},
		icon_ui = function(x,y,w,h,d,c,hover)
			local nmult = 0.8

			local col = getnumcolour(hover,0,c[1])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[1]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[1])), y + h * 0.5 - currfontsize * 0.55 * nmult, nil, nmult, nmult)
		end,
		icon = function(x,y,w,h,d,c,hover)
			local nmult = 0.6

			local col = getnumcolour(hover,0,c[1])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(tostring(d[1]), x + w * 0.5 - currfontsize * 0.25 * nmult * string.len(tostring(d[1])), y + h * 0.5 - currfontsize * 0.55 * nmult, nil, nmult, nmult)
		end,
		func = function(d)
			currtool = d.code
		end,
		update = function(tile,value,slot)
			tile[slot+1] = value
			tile[slot+2] = currcolour
		end,
	},
	["5"] = {
		name = "One letter",
		clear = false,
		layer = 3,
		values = {"a"},
		icon = function(x,y,w,h,d,c,hover)
			local col = getnumcolour(hover,0,c[1])
			love.graphics.setColor(col[1], col[2], col[3], 1)
			love.graphics.print(string.upper(tostring(d[1])), x + w * 0.5 - currfontsize * 0.25 * numbermult * string.len(tostring(d[1])), y + h * 0.5 - currfontsize * 0.55 * numbermult, nil, numbermult, numbermult)
		end,
		func = function(d)
			currtool = d.code
		end,
		update = function(tile,value,slot)
			tile[slot+1] = value
			tile[slot+2] = currcolour
		end,
	},
	["6"] = {
		name = "Curved turn",
		clear = true,
		layer = 4,
		values = {"0","0","0","0"},
		customcolours = true,
		icon = function(x,y,w,h,d,c)
			if (d[1] == "0") and (d[2] == "0") and (d[3] == "0") and (d[4] == "0") then
				love.graphics.setColor(0,0.5,0,1)
				love.graphics.arc( "line", "open", x, y, w * 0.5, 0, math.pi * 0.5, 10 )
			else
				if (d[1] ~= "0") then
					local c = getcolour(d[1])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.arc( "line", "open", x + w, y + h, w * 0.5, math.pi, math.pi * 1.5, 10 )
				end

				if (d[2] ~= "0") then
					local c = getcolour(d[2])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.arc( "line", "open", x + w, y, w * 0.5, math.pi * 0.5, math.pi, 10 )
				end

				if (d[3] ~= "0") then
					local c = getcolour(d[3])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.arc( "line", "open", x, y, w * 0.5, 0, math.pi * 0.5, 10 )
				end

				if (d[4] ~= "0") then
					local c = getcolour(d[4])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.arc( "line", "open", x, y + h, w * 0.5, math.pi * 1.5, math.pi * 2, 10 )
				end
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy,ax,ay,ahx,ahy)
			local cutoff = 0.4

			if (x > hx) and (y > hy) then
				return 2,hx,hy
			elseif (x > hx) and (y < hy) then
				return 3,hx,hy
			elseif (x < hx) and (y < hy) then
				return 4,hx,hy
			elseif (x < hx) and (y > hy) then
				return 5,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("6",ox,oy,z,false,true)
			setsubvalue(currcolour,val,ox,oy,z)
		end,
	},
	["7"] = {
		name = "Jumping line",
		clear = true,
		layer = 1,
		values = {"0","0"},
		customcolours = true,
		icon = function(x,y,w,h,d)
			if (d[1] == "0") and (d[2] == "0") then
				love.graphics.setColor(0,0.5,0,1)
				love.graphics.line(x, y + h * 0.5, x + w * 0.5, y + h * 0.25)
				love.graphics.line(x + w * 0.5, y + h * 0.25, x + w, y + h * 0.5)
			else
				if (d[1] ~= "0") then
					local c = getcolour(d[1])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x, y + h * 0.5, x + w * 0.5, y + h * 0.25)
					love.graphics.line(x + w * 0.5, y + h * 0.25, x + w, y + h * 0.5)
				end

				if (d[2] ~= "0") then
					local c = getcolour(d[2])
					love.graphics.setColor(c[1], c[2], c[3], 1)
					love.graphics.line(x + w * 0.5, y, x + w * 0.75, y + h * 0.5)
					love.graphics.line(x + w * 0.75, y + h * 0.5, x + w * 0.5, y + h)
				end
			end
		end,
		func = function(d)
			currtool = d.code
		end,
		holdcheck = function(x,y,hx,hy)
			if (x > hx) then
				return 2,hx,hy
			elseif (y < hy) then
				return 3,hx,hy
			elseif (x < hx) then
				return 2,hx,hy
			elseif (y > hy) then
				return 3,hx,hy
			else
				return nil,hx,hy
			end
		end,
		holdresult = function(val,ox,oy,nx,ny,z)
			placetile("7",ox,oy,z,false,true)
			setsubvalue(currcolour,val,ox,oy,z)
		end,
	},
}

colours =
{
	a = {
		name = "Black",
		colour = {0, 0, 0},
	},
	b = {
		name = "Dark grey",
		colour = {0.33, 0.33, 0.33},
	},
	c = {
		name = "Light grey",
		colour = {0.66, 0.66, 0.66},
	},
	d = {
		name = "White",
		colour = {0.99, 0.99, 0.99},
	},
	e = {
		name = "Red",
		colour = {0.9, 0.3, 0.2},
	},
	f = {
		name = "Orange",
		colour = {1.0, 0.6, 0.1},
	},
	g = {
		name = "Yellow",
		colour = {1.0, 0.8, 0.1},
	},
	h = {
		name = "Green",
		colour = {0.5, 0.8, 0.2},
	},
	i = {
		name = "Teal",
		colour = {0.2, 0.7, 0.9},
	},
	j = {
		name = "Blue",
		colour = {0.3, 0.35, 1.0},
	},
	k = {
		name = "Purple",
		colour = {0.5, 0.3, 0.8},
	},
	l = {
		name = "Pink",
		colour = {0.8, 0.2, 0.7},
	},
	m = {
		name = "Dark green",
		colour = {0.25, 0.6, 0.1},
	},
}

tileoffsets_layout = {
	"h c g",
	" pko ",
	"dlajb",
	" qmn ",
	"i e f",
}

tileoffsets =
{
	a = {
		name = "Centered",
		offsets = {0,0},
	},
	b = {
		name = "Right edge",
		offsets = {1,0},
	},
	c = {
		name = "Top edge",
		offsets = {0,-1},
	},
	d = {
		name = "Left edge",
		offsets = {-1,0},
	},
	e = {
		name = "Bottom edge",
		offsets = {0,1},
	},
	f = {
		name = "Bottom right edge",
		offsets = {1,1},
	},
	g = {
		name = "Top right edge",
		offsets = {1,-1},
	},
	h = {
		name = "Top left edge",
		offsets = {-1,-1},
	},
	i = {
		name = "Bottom left edge",
		offsets = {-1,1},
	},
	j = {
		name = "Right offset",
		offsets = {0.45,0},
	},
	k = {
		name = "Top offset",
		offsets = {0,-0.45},
	},
	l = {
		name = "Left offset",
		offsets = {-0.45,0},
	},
	m = {
		name = "Bottom offset",
		offsets = {0,0.45},
	},
	n = {
		name = "Bottom right offset",
		offsets = {0.45,0.45},
	},
	o = {
		name = "Top right offset",
		offsets = {0.45,-0.45},
	},
	p = {
		name = "Top left offset",
		offsets = {-0.45,-0.45},
	},
	q = {
		name = "Bottom left offset",
		offsets = {-0.45,0.45},
	},
}