local tooltip = "H - show this help"
.."\n"
.."\n".. "Left mouse - place" -- (some symbols might require dragging with mouse to be placed)
.."\n".. "Right mouse - clear"
.."\n".. "Arrow keys - change puzzle size"
.."\n".. "Control + arrows - change grid size" -- (while leaving the borners intact)
.."\n".. "Shift + arrows - change grid offset" -- (for e.g. nonograms)
.."\n".. "P - print puzzle code on stdout"
.."\n".. "D - print puzzle (lua table) on stdout"
.."\n".. "S - save puzzle to clipboard/file" -- (paste the code somewhere)
.."\n".. "L - load puzzle from clipboard/file" -- (from code)
.."\n".. "Control + S - save puzzle image" -- (goes to users\<your username>\AppData\roaming\LOVE\puzzlemaker\)
.."\n".. "Shift + S - save puzzle image without title or size" -- (goes to users\<your username>\AppData\roaming\LOVE\puzzlemaker\)
.."\n".. "C - clear temporary symbols"
.."\n".. "Control + C - clear all symbols"
.."\n".. "M - switch between symbols to edit" -- (i.e. if you have a cell with multiple numbers, you can use M to cycle through them)
.."\n".. "Number keys - input numbers when hovering over a symbol that uses them"
.."\n".. "Control + number keys - input tens when hovering over a symbol that uses numbers"
.."\n".. "Mousewheel - scroll through numbers when hovering over a symbol that uses them"
.."\n".. "Control + Mousewheel - scroll through tens when hovering over a symbol that uses numbers"
.."\n".. "*/' - input *"
.."\n".. "?/+ - input ?"

return string.gsub(tooltip, '^%s*', '') -- trim whitespace at start
