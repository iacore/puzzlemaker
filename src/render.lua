function love.draw()
	image:renderTo(pre_ui)
	ui_image:renderTo(pre_ui_alt)
	ui_image:renderTo(ui_draw)
	image:renderTo(post_ui)
	image:renderTo(map_draw)
	image:renderTo(puzzle_draw)
	love.graphics.setColor(1, 1, 1, 1)
	love.graphics.draw(image)
	love.graphics.draw(ui_image)
	debugdraw()
	cursor_draw()
end

function pre_ui()
	love.graphics.clear(1,1,1,1)
	love.graphics.setFont(font_title)
	currfontsize = fontsize_title
	love.graphics.setColor(0, 0, 0, 1)
	love.graphics.print(levelname, screenw * 0.5, map.ypos - 38, nil, 1.2, 1.2, #levelname * 0.5 * fontsize_title * 0.5)
	love.graphics.print(tostring(map.subw) .. "x" .. tostring(map.subh), map.xpos, map.ypos - 20, nil, 1.0, 1.0)
end

function pre_ui_alt()
	love.graphics.clear(1,1,1,1)
	love.graphics.setFont(font_hud)
	currfontsize = fontsize_hud
	love.graphics.setColor(0, 0, 0, 1)
end

function post_ui()
	love.graphics.setFont(font)
	currfontsize = fontsize
end

function debugdraw()
	love.graphics.setFont(font_hud)
	love.graphics.setColor(0, 0, 0, 1)
	currfontsize = fontsize_hud
	debuglist_draw()
	debugline_draw()
end

function ui_draw()
	love.graphics.clear(1,1,1,0)
	local mx,my = love.mouse.getPosition()

	for i,v in ipairs(buttons) do
		if (v.code == nil) or ((v.name ~= "tool") and (v.name ~= "offset")) or ((v.name == "tool") and (v.code ~= currtool) and (v.code ~= currcolour)) or ((v.name == "offset") and (v.code ~= curroffset)) then
			love.graphics.setColor(0, 0, 0, 1)
		else
			love.graphics.setColor(1, 0, 0, 1)
		end

		if ((v.name == "grid_lines") and (gridtype == "line")) or ((v.name == "grid_dots") and (gridtype == "dots")) or ((v.name == "grid_cross") and (gridtype == "cross")) or ((v.name == "grid_mountain") and (gridtype == "mountain")) or ((v.name == "grid_dlines") and (gridtype == "dline")) or ((v.name == "grid_no") and (gridtype == "no")) or ((v.name == "grid_dli") and (gridtype == "dli")) then
			love.graphics.setColor(1, 0, 0, 1)
		end

		love.graphics.setLineWidth(4)
		if (mx >= v.x) and (my >= v.y) and (mx < v.x + v.w) and (my < v.y + v.h) then
			love.graphics.rectangle("fill", v.x, v.y, v.w, v.h)

			if (tools[v.code] ~= nil) and (v.name == "tool") then
				local d = tools[v.code]

				love.graphics.print(d.name, 5, 5, nil, 1.0, 1.0)
			elseif (colours[v.code]) and (v.name == "tool") then
				local d = colours[v.code]

				love.graphics.print(d.name, 5, 5, nil, 1.0, 1.0)
			elseif (tileoffsets[v.code] ~= nil) then
				local d = tileoffsets[v.code]

				love.graphics.print(d.name, 5, 5, nil, 1.0, 1.0)
			elseif v.name then
				love.graphics.print(v.name, 5, 5, nil, 1.0, 1.0)
			end
		else
			love.graphics.rectangle("line", v.x, v.y, v.w, v.h)
		end

		love.graphics.setColor(0, 0, 0, 1)
		if (v.icon_ui ~= nil) then
			v.icon_ui(v.x, v.y, v.w, v.h, v.values, {"a"})
		elseif (v.icon ~= nil) then
			v.icon(v.x, v.y, v.w, v.h, v.values, {"a"})
		end
	end
end

function map_draw()
	local mx,my = love.mouse.getPosition()

	local x = map.xpos
	local y = map.ypos

	love.graphics.setColor(0.98, 0.98, 0.98, 1)
	love.graphics.rectangle("fill", x, y, map.xsize, map.ysize)

	local w = math.min(map.w, map.subw)
	local wo = math.ceil((map.w - map.subw) * 0.5)

	local h = math.min(map.h, map.subh)
	local ho = math.ceil((map.h - map.subh) * 0.5)

	local sox = map.suboffsetx
	local soy = map.suboffsety

	if (gridtype == "dots") or (gridtype == "cross") or (gridtype == "no") then
		love.graphics.setColor(0.8, 0.8, 0.8, 1)
		love.graphics.rectangle("line", x + (wo + sox) * tilesize, y + (ho + soy) * tilesize, w * tilesize, h * tilesize)
	elseif (gridtype == "mountain") or (gridtype == "dline") or (gridtype == "dli") then
		love.graphics.setColor(0.25, 0.25, 0.25, 1)
		love.graphics.rectangle("line", x, y, map.xsize, map.ysize)

		if (map.w - map.subw > 0) or (map.h - map.subh > 0) then
			love.graphics.setColor(0.2, 0.2, 0.2, 0.4)
			love.graphics.rectangle("line", x + (wo + sox) * tilesize, y + (ho + soy) * tilesize, w * tilesize, h * tilesize)
		end
	end

	love.graphics.setColor(0, 0, 0, 1)
	love.graphics.setLineWidth(1)

	if (map.w - map.subw > 0) then
		love.graphics.line(x, y, x, y + map.ysize)

		if (map.w - map.subw > 1) then
			love.graphics.line(x + map.xsize, y, x + map.xsize, y + map.ysize)
		end
	end

	if (map.h - map.subh > 0) then
		love.graphics.line(x, y, x, y + ho * tilesize)
		love.graphics.line(x + map.xsize, y, x + map.xsize, y + ho * tilesize)

		love.graphics.line(x, y + (h + ho) * tilesize, x, y + map.ysize)
		love.graphics.line(x + map.xsize, y + (h + ho) * tilesize, x + map.xsize, y + map.ysize)
	end

	if (map.h - map.subh > 0) then
		love.graphics.line(x, y, x + map.xsize, y)

		if (map.h - map.subh > 1) then
			love.graphics.line(x, y + map.ysize, x + map.xsize, y + map.ysize)
		end
	end

	if (map.w - map.subw > 0) then
		love.graphics.line(x, y, x + wo * tilesize, y)
		love.graphics.line(x, y + map.ysize, x + wo * tilesize, y + map.ysize)

		love.graphics.line(x + (w + wo) * tilesize, y, x + map.xsize, y)
		love.graphics.line(x + (w + wo) * tilesize, y + map.ysize, x + map.xsize, y + map.ysize)
	end

	if (gridtype == "line") or (gridtype == "dline") or (gridtype == "dli") then
		if (gridtype == "dline") or (gridtype == "dli") then
			love.graphics.setColor(0, 0, 0, 0.33)
		else
			love.graphics.setColor(0, 0, 0, 0.8)
		end

		for a=wo,w+wo do
			love.graphics.line(x + (a + sox) * tilesize, y + (ho + soy) * tilesize, x + (a + sox) * tilesize, y + (h + ho + soy) * tilesize)
		end

		for a=ho,h+ho do
			love.graphics.line(x + (wo + sox) * tilesize, y + (a + soy) * tilesize, x + (w + wo + sox) * tilesize, y + (a + soy) * tilesize)
		end
	elseif (gridtype == "dots") or (gridtype == "cross") then
		for a=wo,w+wo do
			for b=ho,h+ho do
				local id = a + b * map.w
				local val = (a + b) % 2

				if (gridtype == "cross") then
					love.graphics.setColor(0, 0, 0, 0.8)
				end

				if (gridtype == "dots") or (val == 0) then
					love.graphics.circle("fill", x + (a + sox) * tilesize, y + (b + soy) * tilesize, 3)
				elseif (val == 1) then
					love.graphics.line(x + (a + sox) * tilesize - tilesize / 12.0, y + (b + soy) * tilesize, x + (a + sox) * tilesize + tilesize / 12.0, y + (b + soy) * tilesize)
					love.graphics.line(x + (a + sox) * tilesize, y + (b + soy) * tilesize - tilesize / 12.0, x + (a + sox) * tilesize, y + (b + soy) * tilesize + tilesize / 12.0)
				end
			end
		end
	end

	love.graphics.setColor(0.45, 0.45, 0.45, 1)

	if (gridtype == "mountain") or (gridtype == "dline") then
		for a=wo,w+wo-1 do
			for b=ho,h+ho-1 do
				local id = a + b * map.w

				love.graphics.circle("fill", x + (a + sox) * tilesize + tilesize * 0.5, y + (b + soy) * tilesize + tilesize * 0.5, 3)
			end
		end
	end
end

function cursor_draw()
	local mx,my = love.mouse.getPosition()

	local x = map.xpos
	local y = map.ypos

	if (mx >= x) and (my >= y) and (mx < x + map.xsize) and (my < y + map.ysize) then
		local mxt = math.floor((mx - x) / tilesize)
		local myt = math.floor((my - y) / tilesize)

		local tileid = mxt + myt * map.w

		if (map.layout[tileid] == nil) then
			love.graphics.setColor(0.5, 0.5, 0.5, 0.5)
		else
			love.graphics.setColor(0.5, 0.5, 0.5, 0.25)
		end
		love.graphics.rectangle("fill", x + mxt * tilesize, y + myt * tilesize, tilesize, tilesize)
	end
end

function puzzle_draw()
	local passes =
	{
		{1,5,6},
		{2,7},
		{3,4}
	}

	for i,v in ipairs(passes) do
		puzzle_draw_pass(v)
	end
end

function puzzle_draw_pass(data)
	for a,b in pairs(map.layout) do
		local tx = a % map.w
		local ty = math.floor(a / map.w)

		for i,cl in ipairs(data) do
			local f = b[cl]

			if (f ~= nil) then
				puzzle_draw_do(f,cl,tx,ty)
			end
		end
	end
end

function puzzle_draw_do(f,cl,tx,ty)
	local tdata = tools[f[1]]
	local x = map.xpos
	local y = map.ypos
	local mx,my = love.mouse.getPosition()

	if (tdata ~= nil) then
		if (tdata.icon ~= nil) then
			local extra = {}
			local colourdata = {}
			local offsetdata = ""

			if (#f > 3) then
				local extralimit = #f - math.floor((#f - 1) / 2)-1
				if (tdata.customcolours ~= nil) then
					extralimit = #f-1
				end

				for h=2,extralimit do
					table.insert(extra, f[h])
				end

				if (extralimit < #f-1) and (tdata.customcolours == nil) then
					for h=extralimit+1,#f-1 do
						if (h > 1) then
							table.insert(colourdata, f[h])
						end
					end
				end
			elseif (tdata.customcolours == nil) then
				colourdata = {f[2]}
			end

			offsetdata = f[#f]

			local ox,oy = 0,0
			if (tileoffsets[offsetdata] ~= nil) then
				local odat = tileoffsets[offsetdata]

				ox = odat.offsets[1]
				oy = odat.offsets[2]
			end

			local hover = false
			local hm = latesttile[2]
			if (mx >= x + tx * tilesize) and (mx < x + tx * tilesize + tilesize) and (my >= y + ty * tilesize) and (my < y + ty * tilesize + tilesize) then
				hover = true

				if (f[1] ~= latesttile[1]) then
					latesttile[2] = 0
				end
			end

			if (#colourdata == 1) then
				local c = getcolour(colourdata[1])
				love.graphics.setColor(c[1], c[2], c[3], 1)
			else
				love.graphics.setColor(0, 0, 0, 1)
			end

			love.graphics.setLineWidth(4)
			tdata.icon(x + tx * tilesize + ox * tilesize * 0.5, y + ty * tilesize + oy * tilesize * 0.5, tilesize, tilesize, extra, colourdata, hover)

			latesttile[2] = hm
		end
	end
end