local inspect = require 'lib.inspect'

function love.mousepressed(x, y, button)
	if gottaclear then
		clearerrors()
		gottaclear = false
	end

	if (button == 1) then
		for i,v in ipairs(buttons) do
			if (v.hide == nil) or (v.hide == false) then
				if (x >= v.x) and (y >= v.y) and (x < v.x + v.w) and (y < v.y + v.h) then
					if (v.func ~= nil) then
						v.func(v)
						latesttile[2] = 0
					end
				end
			end
		end
	elseif (button == 2) then
		if (x < map.xpos) or (y < map.ypos) or (x > map.xpos + map.w * tilesize) or (y > map.ypos + map.h * tilesize) then
			currtool = ""
			latesttile[2] = 0
		end
	end
end

function placetile(tool,tx,ty,tz_,clear_,skipcolour_)
	if inbounds(tx,ty) then
		local tileid = tx + ty * map.w
		local l = map.layout
		local tz = tz_ or 1

		local clear = true
		if (clear_ ~= nil) then
			clear = clear_
		end

		local skipcolour = skipcolour_ or false

		if (l[tileid] == nil) then
			l[tileid] = {}
		end

		local d = l[tileid]
		local data = tools[tool]

		if (tool ~= nil) and (data ~= nil) then
			if (d[tz] == nil) or clear or ((d[tz] ~= nil) and (d[tz][1] ~= tool)) then
				d[tz] = {}

				d[tz][1] = tool
				local colourvalues = false

				if (data ~= nil) and (data.values ~= nil) then
					for i,v in ipairs(data.values) do
						table.insert(d[tz], v)
					end

					if (skipcolour == false) then
						colourvalues = true
						for i=1,#data.values do
							table.insert(d[tz], currcolour)
						end
					end
				end

				if (data ~= nil) then
					if (colourvalues == false) and (data.customcolours == nil) then
						table.insert(d[tz], currcolour)
					end
				end
			else
				d[tz][1] = tool
			end

			local offsetslot = 3
			if (data ~= nil) then
				if (data.customcolours ~= nil) then
					offsetslot = 2
				end

				if (data.values ~= nil) then
					offsetslot = 2
					offsetslot = offsetslot + #data.values

					if (data.customcolours == nil) then
						offsetslot = offsetslot + #data.values
					end
				end
			end

			d[tz][offsetslot] = curroffset
		elseif (tool ~= nil) and (data == nil) then
			alert("Tool " .. tostring(tool) .. " doesn't exist?")
		else
			d[tz] = nil
		end

		local found = false
		for i=1,layers do
			if (d[i] ~= nil) then
				found = true
				break
			end
		end

		if (found == false) then
			l[tileid] = nil
		end

		latesttile[1] = tool
	end
end

function inbounds(x,y)
	return (x >= 0) and (y >= 0) and (x < map.w) and (y < map.h)
end

function love.update(delta)
	love.window.setTitle("Puzzle Creator! " .. tostring(love.timer.getFPS()))
	--debugline = getlevelcode()

	local b1 = love.mouse.isDown(1)
	local b2 = love.mouse.isDown(2)

	if (b1 == false) then
		xhold = -1
		yhold = -1
		axhold,ayhold = -1,-1
	end

	local sx = map.xpos
	local sy = map.ypos

	if b1 or b2 then
		local x,y = love.mouse.getPosition()

		if (x >= sx - tilesize * 0.5) and (y >= sy - tilesize * 0.5) and (x < sx + map.xsize + tilesize * 0.5) and (y < sy + map.ysize + tilesize * 0.5) then
			local ax = x - sx
			local ay = y - sy
			local tx = math.floor(ax / tilesize)
			local ty = math.floor(ay / tilesize)

			if (xhold == -1) and (yhold == -1) and inbounds(tx,ty) then
				xhold = tx
				yhold = ty
				axhold = tx * tilesize + tilesize * 0.5
				ayhold = ty * tilesize + tilesize * 0.5
			end

			if b1 then
				local v = tools[currtool] or {}
				currlayer = v.layer or currlayer

				if (v.holdcheck == nil) then
					if inbounds(tx,ty) then
						placetile(currtool,tx,ty,currlayer)
					end
				else
					local r,rx,ry,nx_,ny_ = v.holdcheck(tx,ty,xhold,yhold,ax,ay,axhold,ayhold)

					if (r ~= nil) then
						xhold = tx
						yhold = ty
						axhold = tx * tilesize + tilesize * 0.5
						ayhold = ty * tilesize + tilesize * 0.5

						local nx = nx_ or rx
						local ny = ny_ or ry
						v.holdresult(r,nx,ny,tx,ty,currlayer)
					end
				end
			elseif b2 then
				local v = tools[currtool] or {}
				currlayer = v.layer or currlayer

				if inbounds(tx,ty) then
					placetile(nil,tx,ty,currlayer)
				end
			end
		end
	end
end

function setsubvalue(val,slot,x,y,z)
	local tileid = x + y * map.w
	local d = map.layout[tileid]

	if (d ~= nil) then
		if (d[z] ~= nil) then
			local data = d[z]

			if (data[slot] ~= nil) then
				data[slot] = val
			end
		end
	end
end

function love.wheelmoved(wx, wy)
	local x,y = love.mouse.getPosition()

	local sx = map.xpos
	local sy = map.ypos

	if (x >= sx) and (y >= sy) and (x < sx + map.xsize) and (y < sy + map.ysize) then
		local tx = math.floor((x - sx) / tilesize)
		local ty = math.floor((y - sy) / tilesize)

		local tileid = tx + ty * map.w
		if (map.layout[tileid] ~= nil) then
			local l = map.layout[tileid]

			if (l[3] ~= nil) then
				local d = l[3]
				local code = d[1]

				local data = tools[code]

				local key = d[latesttile[2]+2]

				if (tonumber(key) ~= nil) then
					local newval = tonumber(key)
					local tens = math.floor(newval / 10)
					local ones = math.floor(newval % 10)

					if (love.keyboard.isDown("lctrl") == false) then
						if (wy < 0) then
							ones = ((ones - 1) + 10) % 10
						elseif (wy > 0) then
							ones = ((ones + 1) + 10) % 10
						end
					else
						if (wy < 0) then
							tens = ((tens - 1) + 10) % 10
						elseif (wy > 0) then
							tens = ((tens + 1) + 10) % 10
						end
					end

					newval = math.min(tens * 10 + ones, maxvalue)

					key = tostring(newval)

					if (data.update ~= nil) then
						if (latesttile[1] ~= code) then
							latesttile[2] = 0
						end

						data.update(d,key,latesttile[2]+1)

						latesttile[1] = code
					end
				end
			end
		end
	end
end

function love.keypressed(key)
	local something_happened = false

	if (string.len(key) == 1) and (string.find(validsymbols, key) ~= nil) or (key == "+") or (key == "'") or (key == "m") then
		local x,y = love.mouse.getPosition()

		local sx = map.xpos
		local sy = map.ypos

		if (x >= sx) and (y >= sy) and (x < sx + map.xsize) and (y < sy + map.ysize) then
			local tx = math.floor((x - sx) / tilesize)
			local ty = math.floor((y - sy) / tilesize)

			local tileid = tx + ty * map.w
			if (map.layout[tileid] ~= nil) then
				local l = map.layout[tileid]

				if (l[3] ~= nil) then
					local d = l[3]
					local code = d[1]

					local data = tools[code]

					local key_ = key

					if (key == "+") then
						key_ = "?"
					elseif (key == "'") then
						key_ = "*"
					end

					if (data.update ~= nil) and (data.values ~= nil) then
						if (latesttile[1] ~= code) then
							latesttile[2] = 0
						end

						if (data.values[latesttile[2]+1] ~= nil) and (((data.values[latesttile[2]+1] == "?") and (tonumber(key_) ~= nil)) or (data.values[latesttile[2]+1] == "a") or (key == "m")) then
							if (key ~= "m") then
								if (tonumber(key_) ~= nil) and love.keyboard.isDown("lctrl") then
									data.update(d,math.min(tonumber(key_) * 10, maxvalue),latesttile[2]+1)
								else
									data.update(d,key_,latesttile[2]+1)
								end
							end

							something_happened = true

							latesttile[2] = (latesttile[2] + 1) % #data.values
							latesttile[1] = code
						end
					end
				end
			end
		end
	end

	if (something_happened == false) then
		if (key == "h") then
			show_controls()
		elseif (key == "d") then
			print(inspect(getleveldata()))
		elseif (key == "p") then
			print(getlevelcode())
		elseif (key == "s") then
			if (love.keyboard.isDown("lctrl") == false) and (love.keyboard.isDown("lshift") == false) then
				if savefile then
					dump_file(savefile)
				else
					saveclip()
				end
			else
				saveimage(love.keyboard.isDown("lshift"))
			end
		elseif (key == "l") then
			if savefile == nil then
				local clipb = love.system.getClipboardText()
				if (string.len(clipb) > 0) then
					loadclip(clipb)
				end
			else
				load_file(savefile)
			end
		elseif (key == "down") then
			if love.keyboard.isDown("lctrl") then
				submap_set(math.max(math.min(map.subw, map.w), 3),math.max(math.min(map.subh-1, map.h), 3))
			elseif love.keyboard.isDown("lshift") then
				map.suboffsety = map.suboffsety + 1
			else
				map_setup(map.w,math.max(map.h-1, 3))
			end
		elseif (key == "up") then
			if love.keyboard.isDown("lctrl") then
				submap_set(math.max(math.min(map.subw, map.w), 3),math.max(math.min(map.subh+1, map.h), 3))
			elseif love.keyboard.isDown("lshift") then
				map.suboffsety = map.suboffsety - 1
			else
				map_setup(map.w,math.min(map.h+1, 20))
			end
		elseif (key == "left") then
			if love.keyboard.isDown("lctrl") then
				submap_set(math.max(math.min(map.subw-1, map.w), 3),math.max(math.min(map.subh, map.h), 3))
			elseif love.keyboard.isDown("lshift") then
				map.suboffsetx = map.suboffsetx - 1
			else
				map_setup(math.max(map.w-1, 3),map.h)
			end
		elseif (key == "right") then
			if love.keyboard.isDown("lctrl") then
				submap_set(math.max(math.min(map.subw+1, map.w), 3),math.max(math.min(map.subh, map.h), 3))
			elseif love.keyboard.isDown("lshift") then
				map.suboffsetx = map.suboffsetx + 1
			else
				map_setup(math.min(map.w+1, 20),map.h)
			end
		elseif (key == "c") then
			clearmarks(love.keyboard.isDown("lctrl"))
		elseif (key == "e") then
			local data = tools[currtool]

			-- Kaatui kertaalleen eikä toiminut jossain toisessa keississä
			if (data.update ~= nil) then
				latesttile[2] = (latesttile[2] + 1) % #data.values
			end
		end
	end
end

function clearmarks(full_)
	local full = full_ or false

	image:release()
	image = love.graphics.newCanvas()

	for e,f in pairs(map.layout) do
		for a,b in pairs(f) do
			local t = b[1]
			local data = tools[t]

			if (data ~= nil) then
				if full or (data.clear ~= nil) and data.clear then
					local x = math.floor(e % map.w)
					local y = math.floor(e / map.w)
					placetile(nil,x,y,a)
				end
			end
		end
	end
end