local msgpack = require "lib.msgpack"
require("debuglist")
require("tools")
require("input")
require("render")
require("solvers")
require("solver_utils")

function love.load(arg)
	screenw = 1024
	screenh = 768
	smallscreen = false

	local dw,dh = love.window.getDesktopDimensions(1)
	if (dh < 800) then
		screenh = 640
		smallscreen = true
	end

	love.window.setMode(screenw, screenh)
	love.graphics.setBackgroundColor(1, 1, 1, 1)

	fontsize = 24
	font = love.graphics.newFont("Inconsolata.ttf",fontsize,"normal",2)
	love.graphics.setFont(font)

	fontsize_hud = 16
	font_hud = love.graphics.newFont("Inconsolata.ttf",fontsize_hud,"normal",2)

	fontsize_title = 16
	font_title = love.graphics.newFont("Inconsolata.ttf",fontsize_title,"normal",2)

	currfontsize = fontsize

	lookup = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lookup_number = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY"
	validsymbols = "0123456789abcdefghijklmnopqrstuvwxyz"

	xdim = 6
	xpadding = 24
	ypadding = 28
	numbermult = 1.4

	maxvalue = 60

	tilesize = 24
	tilesize_hud = 24
	layers = 7

	gottaclear = false

	currcolour = "a"
	currcolour_shades = {0, 0, 0}
	currtool = "A"
	curroffset = "a"
	gridtype = "line"
	latesttile = {"",0}

	buttons = {}

	xhold,yhold = -1,-1
	axhold,ayhold = -1,-1
	currlayer = 1

	levelname = "Some puzzle"
	toolbar_setup()
	map_setup(8,8)
	--- @type string?
	savefile = arg[1]

	if savefile ~= nil then
		load_file(savefile)
	end

	love.graphics.setLineStyle( "smooth" )

	image = love.graphics.newCanvas()
	ui_image = love.graphics.newCanvas()

	print("Löve2D paper puzzle maker - https://git.envs.net/iacore/puzzlemaker")
	print("Press H to show help")
	alert("Press H to show help", 500)
end

function show_controls()
	local tooltip = require("hint_controls")
	alert(tooltip, 1000)
end

function createbutton(x,y,w,h,name,text,func,icon,icon_ui)
	table.insert(buttons, {})
	local button = buttons[#buttons]

	button.id = #buttons
	button.name = name
	button.x = x
	button.y = y
	button.w = w
	button.h = h
	button.text = text or ""
	button.data = {}

	button.func = func
	button.icon = icon
	button.icon_ui = icon_ui

	return button
end

function map_setup(w_,h_)
	map = {}
	map.w = w_
	map.h = h_
	map.subw = w_
	map.subh = h_

	tilesize = 48

	if smallscreen then
		if (w_ > 20) or (h_ > 18) then
			tilesize = 25
		elseif (w_ > 17) or (h_ > 14) then
			tilesize = 28
		elseif (w_ > 12) or (h_ > 10) then
			tilesize = 36
		end
	else
		if (w_ > 17) or (h_ > 17) then
			tilesize = 30
		elseif (w_ > 12) or (h_ > 13) then
			tilesize = 36
		end
	end

	if (fontsize ~= tilesize * 0.5) then
		fontsize = tilesize * 0.5
		font = love.graphics.newFont("Inconsolata.ttf",fontsize,"normal",2)
		love.graphics.setFont(font)
	end

	map.xmid = screenw * 0.5
	map.ymid = screenh * 0.5 - ypadding
	map.xpos = map.xmid - map.w * 0.5 * tilesize
	map.ypos = map.ymid - map.h * 0.5 * tilesize
	map.xsize = map.w * tilesize
	map.ysize = map.h * tilesize
	map.subxsize = map.subw * tilesize
	map.subysize = map.subh * tilesize
	map.suboffsetx = 0
	map.suboffsety = 0
	map.layout = {}
end

function submap_set(w,h)
	map.subw = w
	map.subh = h
	map.subxsize = map.subw * tilesize
	map.subysize = map.subh * tilesize
end

function toolbar_setup()
	local toolids = {"A", "I", "G", "Y", "H", "1", "X", "R", "T", "B", "C", "D", "E", "4", "W", "Z", "K", "L", "V", "2", "F", "M", "3", "O", "N", "0", "P", "J", "Q", "S", "U", "5", "6", "7", "8", "9", "a", "b", "c"}
	toolbarwidth = 20
	toolbarheight = 7
	toolbarsize = 36

	local w,h = love.graphics.getDimensions()

	local i_ = 1

	local switch_to_side = false

	for i,b in ipairs(toolids) do
		if (tools[b] ~= nil) then
			local v = tools[b]
			local x = tilesize * 0.25 + ((i_ - 1) % toolbarwidth) * (toolbarsize + 8)
			local y = h - 86 + math.floor((i_ - 1) / toolbarwidth) * (toolbarsize + 8)

			if switch_to_side then
				x = tilesize * 0.25 + math.floor((i_ - 1) % 2) * (toolbarsize + 8)
				y = h - 130 - ((i_ - 1) / 2) * (toolbarsize + 8)
			end

			local text = v.text or ""

			local button = createbutton(x,y,toolbarsize,toolbarsize,"tool",text,v.func,v.icon,v.icon_ui)
			v.buttonid = button.id
			button.values = v.values
			button.code = b

			i_ = i_ + 1

			if (i >= toolbarwidth * 2) then
				switch_to_side = true
				i_ = 1
			end
		end
	end

	local colour_icon = function(x,y,w,h,d) love.graphics.setColor(d[1], d[2], d[3], 1) love.graphics.rectangle("fill", x + 2, y + 2, w - 4, h - 4) end
	local colour_func = function(d) currcolour = d.code end
	local colourids = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q"}
	i_ = 1

	for i,b in ipairs(colourids) do
		if (colours[b] ~= nil) then
			local v = colours[b]
			local x = w - 86 + math.floor((i_ - 1) / toolbarheight) * (toolbarsize + 8)
			local y = h - 306 + ((i_ - 1) % toolbarheight) * (toolbarsize + 8)

			local text = v.text or ""

			local button = createbutton(x,y,toolbarsize,toolbarsize,"tool",text,colour_func,colour_icon)
			v.buttonid = button.id
			button.values = v.colour
			button.code = b

			i_ = i_ + 1
		end
	end

	local offsetsize = toolbarsize * 0.6
	local offsets_icon = function(x,y,w,h,d) love.graphics.circle("fill", x + w * 0.5 + offsetsize * 0.5 * d[1], y + w * 0.5 + offsetsize * 0.5 * d[2], 6) end
	local offsets_func = function(d) curroffset = d.code end
	for i,row in ipairs(tileoffsets_layout) do
		for j = 1, #row do
			local b = row:sub(j,j)
			if b == " " then
				goto continue
			end
			local v = tileoffsets[b]
			assert(v, "Invalid offset: " .. i .. "," .. j .. "," .. b)
			local x = w - (offsetsize + 8) - (#row - j) * (offsetsize + 8)
			local y = offsetsize * 0.5 + (i-1) * (offsetsize + 8)

			local text = v.name or ""

			local button = createbutton(x,y,offsetsize,offsetsize,"offset",text,offsets_func,offsets_icon)
			v.buttonid = button.id
			button.values = v.offsets
			button.code = b
		    ::continue::
		end
	end

	local x = w - 96
	local y = h - 296 - toolbarsize * 1

	local dots_name = "Dots"
	local dots_icon = function(x,y,w,h,d) love.graphics.print(dots_name, x + toolbarsize * 1.25 - #dots_name * 4, y + toolbarsize * 0.25 - 8) end
	local dots_func = function(d) gridtype = "dots" end
	createbutton(x,y,toolbarsize * 2.5,toolbarsize * 0.5,"grid_dots",dots_name,dots_func,dots_icon)

	y = y - toolbarsize * 0.5 - 8

	local stitch_name = "Stitch"
	local stitch_icon = function(x,y,w,h,d) love.graphics.print(stitch_name, x + toolbarsize * 1.25 - #stitch_name * 4, y + toolbarsize * 0.25 - 8) end
	local stitch_func = function(d) gridtype = "cross" end
	createbutton(x,y,toolbarsize * 2.5,toolbarsize * 0.5,"grid_cross",stitch_name,stitch_func,stitch_icon)

	y = y - toolbarsize * 0.5 - 8

	local climb_name = "Climb"
	local climb_icon = function(x,y,w,h,d) love.graphics.print(climb_name, x + toolbarsize * 1.25 - #climb_name * 4, y + toolbarsize * 0.25 - 8) end
	local climb_func = function(d) gridtype = "mountain" end
	createbutton(x,y,toolbarsize * 2.5,toolbarsize * 0.5,"grid_mountain",climb_name,climb_func,climb_icon)

	y = y - toolbarsize * 0.5 - 8

	local lines_name = "Lines"
	local lines_icon = function(x,y,w,h,d) love.graphics.print(lines_name, x + toolbarsize * 1.25 - #lines_name * 4, y + toolbarsize * 0.25 - 8) end
	local lines_func = function(d) gridtype = "line" end
	createbutton(x,y,toolbarsize * 2.5,toolbarsize * 0.5,"grid_lines",lines_name,lines_func,lines_icon)

	y = y - toolbarsize * 0.5 - 8

	local dli_name = "Dim lines"
	local dli_icon = function(x,y,w,h,d) love.graphics.print(dli_name, x + toolbarsize * 1.25 - #dli_name * 4, y + toolbarsize * 0.25 - 8) end
	local dli_func = function(d) gridtype = "dli" end
	createbutton(x,y,toolbarsize * 2.5,toolbarsize * 0.5,"grid_dli",dli_name,dli_func,dli_icon)

	y = y - toolbarsize * 0.5 - 8

	local dlines_name = "Lines/dots"
	local dlines_icon = function(x,y,w,h,d) love.graphics.print(dlines_name, x + toolbarsize * 1.25 - #dlines_name * 4, y + toolbarsize * 0.25 - 8) end
	local dlines_func = function(d) gridtype = "dline" end
	createbutton(x,y,toolbarsize * 2.5,toolbarsize * 0.5,"grid_dlines",dlines_name,dlines_func,dlines_icon)

	y = y - toolbarsize * 0.5 - 8

	local no_name = "Nothing"
	local no_icon = function(x,y,w,h,d) love.graphics.print(no_name, x + toolbarsize * 1.25 - #no_name * 4, y + toolbarsize * 0.25 - 8) end
	local no_func = function(d) gridtype = "no" end
	createbutton(x,y,toolbarsize * 2.5,toolbarsize * 0.5,"grid_no",no_name,no_func,no_icon)

	i_ = 1

	--[[
	for i,b in ipairs(toolids) do
		if (solvers[b] ~= nil) then
			local v = solvers[b]
			local x = 8
			local y = h - 112 - i_ * (toolbarsize * 0.5 + 8)
			
			local nmult = 0.75
			local icon = function(x,y,w,h,d) love.graphics.print(v.name, x + toolbarsize * 2 - #v.name * 6 * nmult, y + toolbarsize * 0.25 - 12 * nmult, nil, nmult, nmult) end
			
			local button = createbutton(x,y,toolbarsize * 4,toolbarsize * 0.5,"solver",v.name,v.func,icon)
			v.buttonid = button.id
			button.code = b
			
			i_ = i_ + 1
		end
	end
	]]--

	local name_name = "Rename"
	local name_icon = function(x,y,w,h,d) love.graphics.print(name_name, x + toolbarsize - #name_name * 4, y + toolbarsize * 0.5 - 8) end
	local name_func = function(d) local clipb = string.gsub(love.system.getClipboardText(), "%p+", "") if (#clipb > 0) then levelname = love.system.getClipboardText() end end
	createbutton(w - 166,h - 130,toolbarsize * 2,toolbarsize,"rename",name_name,name_func,name_icon)
end

function loadclip(clip_)
	local fclip = clip_
	local i = 1

	for clip in string.gmatch(fclip, "[^_]+") do
		if (i == 1) then
			levelname = clip
		elseif (i == 2) then
			if (clip ~= "d") and (clip ~= "l") and (clip ~= "c") and (clip ~= "m") and (clip ~= "dl") and (clip ~= "n") and (clip ~= "dli") then
				--loadclip_old(clip_)
				alert("Old level format",100)
				break
			elseif (clip == "d") then
				gridtype = "dots"
			elseif (clip == "l") then
				gridtype = "line"
			elseif (clip == "c") then
				gridtype = "cross"
			elseif (clip == "m") then
				gridtype = "mountain"
			elseif (clip == "dl") then
				gridtype = "dline"
			elseif (clip == "n") then
				gridtype = "no"
			elseif (clip == "dli") then
				gridtype = "dli"
			end
		elseif (i >= 3) then
			if (#clip > 4) and (i == 3) then
				local w = tonumber(string.sub(clip, 1, 2)) or 8
				local h = tonumber(string.sub(clip, 3, 4)) or 8
				clip = string.sub(clip, 5)
				map_setup(w,h)

				local w0 = tonumber(string.sub(clip, 1, 2))
				local h0 = tonumber(string.sub(clip, 3, 4))
				if (w0 ~= nil) and (h0 ~= nil) then
					w = w0
					h = h0

					submap_set(w,h)

					clip = string.sub(clip, 5)

					local w1 = tonumber(string.sub(clip, 1, 2))
					local h1 = tonumber(string.sub(clip, 3, 4))
					if (w1 ~= nil) and (h1 ~= nil) then
						w = w1
						h = h1

						map.suboffsetx = w
						map.suboffsety = h

						clip = string.sub(clip, 5)
					end
				end
			end

			local tileid = 0
			local readslot = 1

			if (#clip > 0) then
				local streak = false
				while (tileid < map.w * map.h) do
					local l = string.sub(clip, readslot, readslot)

					if (l ~= nil) then
						if (tonumber(l) == nil) and (string.find(lookup, tostring(l)) ~= nil) then
							tileid = tileid + string.find(lookup, tostring(l))
							readslot = readslot + 1

							if (streak == false) then
								streak = true
							else
								tileid = tileid + 1
							end
						elseif (l ~= ",") then
							streak = false
							local layer = tonumber(l)
							local skip = 3
							local t = string.sub(clip, readslot+1, readslot+1)
							local tool = tools[t]
							local colourdata = {}
							local offsetslot = 3

							prevlayer = layer

							if (tool ~= nil) then
								local subdata = {}

								if (tool.values ~= nil) then
									skip = 2

									if (#tool.values > 0) then
										for j=1,#tool.values do
											local sd = string.sub(clip, readslot+1+j, readslot+1+j)

											if (tonumber(sd) == nil) and (tool.values[j] == "?") then
												for a=1,#lookup_number do
													if (string.sub(lookup_number, a, a) == sd) then
														sd = tostring(a-1)
														break
													end
												end
											end

											subdata[j] = sd
											skip = skip + 1
											offsetslot = offsetslot + 1
										end

										if (tool.customcolours == nil) then
											for j=1,#tool.values do
												local sd = string.sub(clip, readslot+1+j+#subdata, readslot+1+j+#subdata)

												colourdata[j] = sd
												skip = skip + 1
												offsetslot = offsetslot + 1
											end
										end
									end

									offsetslot = offsetslot - 1
								else
									local c = string.sub(clip, readslot+2, readslot+2)
									colourdata[1] = c
								end

								local tx = math.floor(tileid % map.w)
								local ty = math.floor(tileid / map.w)

								if (tool.customcolours == nil) then
									placetile(t,tx,ty,layer)
								else
									placetile(t,tx,ty,layer,nil,true)
								end

								--alert("Placing " .. tostring(t) .. " at " .. tostring(tx) .. ", " .. tostring(ty) .. " on layer " .. tostring(layer),80)

								for a,b in ipairs(subdata) do
									setsubvalue(b,a+1,tx,ty,layer)
								end

								for a,b in ipairs(colourdata) do
									setsubvalue(b,a+1+#subdata,tx,ty,layer)
								end

								local od = string.sub(clip, readslot+offsetslot, readslot+offsetslot)
								if (tileoffsets[od] ~= nil) then
									setsubvalue(od,offsetslot,tx,ty,layer)
									skip = skip + 1
								else
									setsubvalue("a",offsetslot,tx,ty,layer)
								end
							end

							readslot = readslot + skip
						elseif (l == ",") then
							readslot = readslot + 1
							tileid = tileid + 1
						end
					end
				end
			end
		end

		i = i + 1
	end
end

function load_file(filename)
	local f = io.open(filename, "r")
	if f ~= nil then
		local encoded = f:read("a")
		f:close()
		local success, err_or_data = msgpack.unpack(encoded)
		if success then
			loadleveldata(err_or_data)
		else
			alert(err_or_data, 2000)
		end
	end
end

function dump_file(filename)
	local encoded, err = msgpack.pack(getleveldata())
	if encoded == nil then
		alert(err, 2000)
	else
		local f, err = io.open(filename, "wb")
		if f == nil then
			alert(err, 2000)
		else
			f:write(encoded)
			f:close()
			alert("Saved as msgpack to '" .. savefile .. "'", 500)
		end
	end
end

function saveclip()
	result = getlevelcode()

	love.system.setClipboardText(result)
end

function getletter(number)
	local result = string.sub(lookup_number, number+1, number+1)

	return result
end


function getleveldata()
	return {
		levelname = levelname,
		map = map,
		gridtype = gridtype,
	}
end

local function genlevelcode(level)
	local map = level.map
	local levelname = level.levelname
	local gridtype = level.gridtype

	local result = ""
	local tilecount = map.w * map.h
	local l = map.layout

	local gt = "l"
	if (gridtype == "dots") then
		gt = "d"
	elseif (gridtype == "cross") then
		gt = "c"
	elseif (gridtype == "mountain") then
		gt = "m"
	elseif (gridtype == "dline") then
		gt = "dl"
	elseif (gridtype == "no") then
		gt = "n"
	elseif (gridtype == "dli") then
		gt = "dli"
	end

	result = levelname .. "_" .. gt .. "_"

	result = result .. string.sub("00" .. tostring(map.w), -2) .. string.sub("00" .. tostring(map.h), -2)

	if (map.subw ~= map.w) or (map.subh ~= map.h) then
		result = result .. string.sub("00" .. tostring(map.subw), -2) .. string.sub("00" .. tostring(map.subh), -2)

		if (map.suboffsetx ~= 0) or (map.suboffsety ~= 0) then
			result = result .. string.sub("00" .. tostring(map.suboffsetx), -2) .. string.sub("00" .. tostring(map.suboffsety), -2)
		end
	end

	local streak = 0

	for j=0,tilecount-1 do
		local empty = true
		local addition = ""

		if (l[j] ~= nil) then
			local layer = l[j]

			for i=1,layers do
				local tdata = layer[i]

				if (tdata ~= nil) then
					empty = false
					local tool = tdata[1]

					addition = addition .. tostring(i) .. tool

					if (#tdata > 1) then
						for a,b in ipairs(tdata) do
							if (a > 1) then
								if (tonumber(b) ~= nil) then
									addition = addition .. getletter(tonumber(b))
								else
									addition = addition .. b
								end
							end
						end
					end
				end
			end
		end

		if empty then
			streak = streak + 1

			if (streak > string.len(lookup)) then
				result = result .. string.sub(lookup, -1)
				streak = 0
			end
		else
			if (streak > 0) then
				result = result .. string.sub(lookup, streak, streak)
			end
			streak = 0
		end

		if (#addition > 0) then
			result = result .. addition .. ","
		end
	end

	return result
end

function getlevelcode()
	local leveldata = getleveldata()
	return genlevelcode(leveldata)
end

function loadleveldata(level)
	map = level.map
	levelname = level.levelname
	gridtype = level.gridtype
end

function getnumcolour(hover,slot,colour_)
	local c = {0,0,0,1}

	if (colour_ ~= nil) then
		c = getcolour(colour_)
		table.insert(c, 1)
	end

	if (hover ~= nil) and hover then
		if (latesttile[2] == slot) then
			c = {1,0,0,1}
		end
	end

	return c
end

function getcolour(c)
	local data = colours[c]

	if (data ~= nil) then
		return data.colour
	end

	return {0,0,0}
end

function saveimage(shiftdown)
	local test

	if (shiftdown == false) then
		test = image:newImageData( nil, nil, map.xpos - 12, map.ypos - 38, map.xsize + 24, map.ysize + 50)
	else
		test = image:newImageData( nil, nil, map.xpos - 4, map.ypos - 4, map.xsize + 8, map.ysize + 8)
	end

	local name = levelname
	name = string.gsub(name, "%p+", "")
	if (#name == 0) then
		name = "puzzle"
	end
	local testdata = test:encode( "png", name .. ".png" )

	test:release()
	testdata:release()
end
