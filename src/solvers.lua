solvers =
{
	A =
	{
		name = "Celltinels",
		func = function()
			clearerrors()

			local nums = getcells({"B"})
			local cells = getcells({"A"})
			local success = true

			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)

				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"A"},{"B","K","L"})

				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end

			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)

				local valid = snakecheck_cells(x,y,{"A"})

				if (valid == false) then
					placetile("G",x,y,1)
					success = false
				end
			end

			if (#cells > 0) then
				local disconnect = connectivity_cells(cells,true)

				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	B =
	{
		name = "Sentinels",
		func = function()
			clearerrors()

			local nums = getcells({"B"})
			local cells = getcells({"H","X"})
			local success = true

			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)

				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"H","X"},{"B","K","L"})

				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end

			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)

				local count = countconnections(x,y)

				if (count ~= 2) then
					placetile("G",x,y,1)
					success = false
				end
			end

			if (#cells > 0) then
				local disconnect = connectivity_line(cells,true)
				local uturn,utiles = u_turn(cells)

				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				elseif uturn then
					success = false
					for i,v in ipairs(utiles) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	C =
	{
		name = "Sentinal View",
		func = function()
			clearerrors()

			local nums = getcells({"B"})
			local cells = getcells({"H","X"})
			local success = true

			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)

				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"H","X"},{"B","K","L"},true)

				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end

			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)

				local count = countconnections(x,y)

				if (count ~= 2) then
					placetile("G",x,y,1)
					success = false
				end
			end

			if (#cells > 0) then
				local disconnect = connectivity_line(cells,true)
				local uturn,utiles = u_turn(cells)

				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				elseif uturn then
					success = false
					for i,v in ipairs(utiles) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	D =
	{
		name = "Snakcelltinels",
		func = function()
			clearerrors()

			local nums = getcells({"B"})
			local cells = getcells({"A"})
			local success = true

			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)

				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"A"},{"B","K","L"})

				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end

			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)

				local valid = snakecheck_cells(x,y,{"A"})

				if (valid == false) then
					placetile("G",x,y,1)
					success = false
				end
			end

			if (#cells > 0) then
				local disconnect = connectivity_cells(cells,false,{"M","F"})

				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	E =
	{
		name = "Ouroboros",
		func = function()
			clearerrors()

			local nums = getcells({"C","W"})
			local cells = getcells({"H","X"})
			local success = true

			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)

				local allowed = tonumber(v[3]) or -1
				local allowed2 = tonumber(v[2]) or -1
				local count = countcells_surround(x,y,{"H","X"},1)

				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				elseif (allowed2 > 0) and (count == 0) then
					placetile("G",x,y,1)
					success = false
				elseif (allowed2 == 0) and (count ~= 0) then
					placetile("G",x,y,1)
					success = false
				end
			end

			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)

				local count = countconnections(x,y)

				if (count ~= 2) and (count ~= 1) then
					placetile("G",x,y,1)
					success = false
				end
			end

			if (#cells > 0) then
				local snakes,snakemap = getsnakes_line(cells)

				local valid,issues = ouroboros_check(snakes,snakemap)

				if (valid == false) then
					success = false
					for i,v in ipairs(issues) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end

				for i,v in ipairs(nums) do
					local x,y = getpos(v.pos)

					local allowed = tonumber(v[2]) or -1
					local dirs = {{1,0},{0,-1},{-1,0},{0,1},{1,1},{1,-1},{-1,-1},{-1,1}}
					local count = 0
					local seen = {}

					for a,b in ipairs(dirs) do
						local ox,oy = b[1],b[2]

						if inbounds(x+ox,y+oy) then
							local tid = (x + ox) + (y + oy) * map.w

							if (snakemap[tid] ~= nil) then
								local snake_id = snakemap[tid]

								if (seen[snake_id] == nil) then
									count = count + 1
									seen[snake_id] = 1
								end
							end
						end
					end

					if (allowed >= 0) and (count ~= allowed) then
						placetile("G",x,y,1)
						success = false
					end
				end
			end
		end,
	},
	F =
	{
		name = "Canal view",
		func = function()
			clearerrors()

			local nums = getcells({"B"})
			local cells = getcells({"A"})
			local success = true

			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)

				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"A"},{"B","K","L"},true)

				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end

			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)

				local valid = squarecheck(x,y,{"A"})

				if (valid == false) then
					placetile("G",x,y,1)

					if inbounds(x+1,y) then
						placetile("G",x+1,y,1)
					end

					if inbounds(x+1,y+1) then
						placetile("G",x+1,y+1,1)
					end

					if inbounds(x,y+1) then
						placetile("G",x,y+1,1)
					end

					success = false
				end
			end

			if (#cells > 0) then
				local disconnect = connectivity_cells(cells,false)

				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	G =
	{
		name = "Diaganal view",
		func = function()
			clearerrors()

			local nums = getcells({"B"})
			local cells = getcells({"A"})
			local success = true

			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)

				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"A"},{"B","K","L"},true,true)

				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end

			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)

				local valid = squarecheck(x,y,{"A"})

				if (valid == false) then
					placetile("G",x,y,1)

					if inbounds(x+1,y) then
						placetile("G",x+1,y,1)
					end

					if inbounds(x+1,y+1) then
						placetile("G",x+1,y+1,1)
					end

					if inbounds(x,y+1) then
						placetile("G",x,y+1,1)
					end

					success = false
				end
			end

			if (#cells > 0) then
				local disconnect = connectivity_cells(cells,false)

				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	--[[
	H =
	{
		name = "Seaweed",
		func = function()
			clearerrors()
			
			local nums = getcells({"B"})
			local cells = getcells({"H","X"})
			local success = true
			
			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)
				
				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"H","X"},{"B","K","L"})
				
				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)
				
				local count = countconnections(x,y)
				
				if (count ~= 2) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			if success and (#cells > 0) then
				local disconnect = connectivity_line(cells,true)
				
				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	I =
	{
		name = "Crossstitch",
		func = function()
			clearerrors()
			
			local nums = getcells({"B"})
			local cells = getcells({"H","X"})
			local success = true
			
			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)
				
				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"H","X"},{"B","K","L"})
				
				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)
				
				local count = countconnections(x,y)
				
				if (count ~= 2) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			if success and (#cells > 0) then
				local disconnect = connectivity_line(cells,true)
				
				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	J =
	{
		name = "Oddstitch",
		func = function()
			clearerrors()
			
			local nums = getcells({"B"})
			local cells = getcells({"H","X"})
			local success = true
			
			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)
				
				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"H","X"},{"B","K","L"})
				
				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)
				
				local count = countconnections(x,y)
				
				if (count ~= 2) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			if success and (#cells > 0) then
				local disconnect = connectivity_line(cells,true)
				
				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	K =
	{
		name = "Rollercoaster",
		func = function()
			clearerrors()
			
			local nums = getcells({"B"})
			local cells = getcells({"H","X"})
			local success = true
			
			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)
				
				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"H","X"},{"B","K","L"})
				
				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)
				
				local count = countconnections(x,y)
				
				if (count ~= 2) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			if success and (#cells > 0) then
				local disconnect = connectivity_line(cells,true)
				
				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	L =
	{
		name = "Sokosoko",
		func = function()
			clearerrors()
			
			local nums = getcells({"B"})
			local cells = getcells({"H","X"})
			local success = true
			
			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)
				
				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"H","X"},{"B","K","L"})
				
				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)
				
				local count = countconnections(x,y)
				
				if (count ~= 2) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			if success and (#cells > 0) then
				local disconnect = connectivity_line(cells,true)
				
				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	M =
	{
		name = "Hiking track",
		func = function()
			clearerrors()
			
			local nums = getcells({"C","W"})
			local cells = getcells({"H","X"})
			
			local nummap = getmap(nums)
			local cellmap = getmap(cells)
			local success = true
			
			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)
				
				local allowed = tonumber(v[2]) or -1
				local allowed2 = tonumber(v[3]) or -1
				local count = countconnections(x,y)
				
				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				elseif (allowed2 > 0) and (count == 0) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)
				
				local count = countconnections(x,y)
				
				if (count ~= 2) and (nummap[v.pos] == nil) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			if (#cells > 0) then
				local snakes,snakemap = getsnakes_line(cells,nummap)
			end
		end,
	},
	N =
	{
		name = "Mountain climber",
		func = function()
			clearerrors()
			
			local nums = getcells({"B"})
			local cells = getcells({"H","X"})
			local success = true
			
			for i,v in ipairs(nums) do
				local x,y = getpos(v.pos)
				
				local allowed = tonumber(v[2]) or -1
				local count = countcells_radial(x,y,{"H","X"},{"B","K","L"})
				
				if (allowed >= 0) and (count ~= allowed) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			for i,v in ipairs(cells) do
				local x,y = getpos(v.pos)
				
				local count = countconnections(x,y)
				
				if (count ~= 2) then
					placetile("G",x,y,1)
					success = false
				end
			end
			
			if success and (#cells > 0) then
				local disconnect = connectivity_line(cells,true)
				
				if (#disconnect > 0) then
					success = false
					for i,v in ipairs(disconnect) do
						local x,y = getpos(v)
						placetile("G",x,y,1)
					end
				end
			end
		end,
	},
	]]--
}