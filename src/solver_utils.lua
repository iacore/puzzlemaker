function connectivity_cells(data,check_loop_,visits_)
	local found = {}
	local cellmap = {}
	local visitcellmap = {}
	local dirs = {{1,0},{0,-1},{-1,0},{0,1}}

	local start = data[1]["pos"]
	local check = {start}

	for i,v in ipairs(data) do
		cellmap[v.pos] = 1
	end

	local shape = {start}
	local something = false
	local loop = false

	local check_loop = check_loop_ or false

	local visits = visits_ or {}
	local visitcells = getcells(visits)

	for i,v in ipairs(visitcells) do
		visitcellmap[v.pos] = 1
	end

	while (#check > 0) do
		local c = check[1]

		if (found[c] == nil) then
			found[c] = 1

			local x,y = getpos(c)

			for i,v in ipairs(dirs) do
				local ox,oy = v[1],v[2]
				local cx = x + ox
				local cy = y + oy

				if inbounds(cx,cy) then
					local tileid = cx + cy * map.w

					if (cellmap[tileid] ~= nil) and (found[tileid] == nil) then
						table.insert(check, tileid)
						table.insert(shape, tileid)
						something = true

						if check_loop then
							break
						end
					end

					if (cellmap[tileid] ~= nil) and (start == tileid) then
						loop = true
					end
				end
			end
		end

		table.remove(check, 1)
	end

	if something then
		for i,v in pairs(cellmap) do
			if (found[i] == nil) then
				return shape
			end
		end

		if (loop == false) and check_loop then
			return shape
		end

		if (check_loop == false) and (#visits > 0) then
			local fullcount = 0
			local issues = {}

			for i,v in pairs(visitcellmap) do
				if (cellmap[i] ~= nil) then
					local count = countsurrounds(i,cellmap)

					if (count == 1) then
						fullcount = fullcount + 1
					else
						table.insert(issues, i)
					end
				end
			end

			if (fullcount ~= 2) then
				return issues
			end
		end

		for i,v in pairs(visitcellmap) do
			if (cellmap[i] == nil) then
				return {i}
			end
		end
	else
		return {0}
	end

	return {}
end

function connectivity_line(data,check_loop_)
	local found = {}
	local cellmap = {}
	local dirs = {{1,0},{0,-1},{-1,0},{0,1}}

	local start = data[1]["pos"]
	local check = {start}

	for i,v in ipairs(data) do
		cellmap[v.pos] = 1
	end

	local shape = {start}
	local something = false
	local loop = false

	local check_loop = check_loop_ or false

	while (#check > 0) do
		local c = check[1]

		if (found[c] == nil) then
			found[c] = 1

			local x,y = getpos(c)

			for i,v in ipairs(dirs) do
				local ox,oy = v[1],v[2]
				local cx = x + ox
				local cy = y + oy

				if inbounds(cx,cy) and connection_between(x,y,cx,cy) then
					local tileid = cx + cy * map.w

					if (cellmap[tileid] ~= nil) and (found[tileid] == nil) then
						table.insert(check, tileid)
						table.insert(shape, tileid)
						something = true

						if check_loop then
							break
						end
					end

					if (cellmap[tileid] ~= nil) and (start == tileid) then
						loop = true
					end
				end
			end
		end

		table.remove(check, 1)
	end

	if something then
		for i,v in pairs(cellmap) do
			if (found[i] == nil) then
				return shape
			end
		end

		if (loop == false) and check_loop then
			return shape
		end
	else
		return {0}
	end

	return {}
end

function clearerrors()
	cells = getcells({"G"})

	for i,v in ipairs(cells) do
		local x,y = getpos(v.pos)

		placetile(nil,x,y,1)
	end

	gottaclear = true
end

function snakecheck_cells(x,y,targets)
	local dirs = {{1,0},{0,-1},{-1,0},{0,1}}
	local diagonal = {{1,1,1,4},{1,-1,1,2},{-1,-1,2,3},{-1,1,3,4}}

	local count = countcells_surround(x,y,targets,0)

	if (count > 2) then
		return false
	end

	for i,v in ipairs(diagonal) do
		local ox,oy = v[1],v[2]
		local cx = x + ox
		local cy = y + oy

		local t = findcells(cx,cy,targets)
		count = -1

		if (#t > 0) then
			count = 0
			local data = dirs[v[3]]
			ox,oy = data[1],data[2]
			cx = x + ox
			cy = y + oy

			t = {}

			t = findcells(cx,cy,targets)
			if (#t > 0) then
				count = count + 1
			end

			data = dirs[v[4]]
			ox,oy = data[1],data[2]
			cx = x + ox
			cy = y + oy

			t = {}

			t = findcells(cx,cy,targets)
			if (#t > 0) then
				count = count + 1
			end
		end

		if (count == 0) or (count == 2) then
			return false
		end
	end

	return true
end

function squarecheck(x,y,targets)
	local a = findcells(x+1,y,targets)
	local b = findcells(x+1,y+1,targets)
	local c = findcells(x,y+1,targets)

	if (#a > 0) and (#b > 0) and (#c > 0) then
		return false
	end

	return true
end

function countconnections(x,y)
	local dirs = {{1,0,3},{0,-1,4},{-1,0,1},{0,1,2}}

	local count = 0
	local tid1 = x + y * map.w
	local t1 = findcells(x,y,{"H","X"})

	if (#t1 > 0) then
		for i,v in ipairs(dirs) do
			local ox,oy = v[1],v[2]
			local cx = x + ox
			local cy = y + oy

			if inbounds(cx,cy) then
				local tid2 = cx + cy * map.w
				local t2 = findcells(cx,cy,{"H","X"})

				if (#t2 > 0) then
					local s1 = i+1
					local s2 = v[3]+1

					if (t1[s1] ~= "0") and (t2[s2] ~= "0") then
						count = count + 1
					end
				end
			end
		end
	end

	return count
end

function connection_between(x1,y1,x2,y2)
	local t1 = findcells(x1,y1,{"H","X"})
	local t2 = findcells(x2,y2,{"H","X"})
	local dirs = {{1,0,3},{0,-1,4},{-1,0,1},{0,1,2}}

	if (#t1 > 0) and (#t2 > 0) then
		local ox = x2 - x1
		local oy = y2 - y1

		for i,v in ipairs(dirs) do
			if (v[1] == ox) and (v[2] == oy) then
				local s1 = i+1
				local s2 = v[3]+1

				if (t1[s1] ~= "0") and (t2[s2] ~= "0") then
					return true
				else
					return false
				end
			end
		end
	end

	return false
end

function connection_between_tid(tid1,tid2)
	local x1,y1 = getpos(tid1)
	local x2,y2 = getpos(tid2)

	return connection_between(x1,y1,x2,y2)
end

function countpointers(x,y)
	local dirs = {{1,0,2},{0,-1,3},{-1,0,4},{0,1,5}}
	local count = 0

	for i,v in ipairs(dirs) do
		local ox,oy = v[1],v[2]
		local t = findcells(x+ox,y+oy,{"H","X"})

		if (#t > 0) and inbounds(x+ox,y+oy) then
			local success = true

			for a=2,#t do
				if (a ~= v[3]) and (t[a] ~= "0") then
					success = false
				elseif (a == v[3]) and (t[a] == "0") then
					success = false
				end
			end

			if success then
				count = count + 1
			end
		end
	end

	return count
end

function countcells_surround(x,y,targets,style_)
	local dirs = {{1,0},{0,-1},{-1,0},{0,1},{1,1},{1,-1},{-1,-1},{-1,1}}
	local result = 0

	local imin = 1
	local imax = 4

	local style = style_ or 0

	if (style == 1) then
		imin = 1
		imax = 8
	elseif (style == 2) then
		imin = 5
		imax = 8
	end

	for i=imin,imax do
		local v = dirs[i]
		local ox,oy = v[1],v[2]

		local cx = x + ox
		local cy = y + oy

		local t = findcells(cx,cy,targets)

		if (#t > 0) then
			result = result + 1
		end
	end

	return result
end

function getmap(d)
	local result = {}

	for i,v in ipairs(d) do
		result[v.pos] = 1
	end

	return result
end

function getsnakes_line(data)
	local cellmap = getmap(data)
	local result = {}
	local resultmap = {}

	local found = {}
	local heads = {}

	for i,v in pairs(cellmap) do
		local count = countsurrounds(i,cellmap,true)

		if (count == 1) then
			table.insert(heads, i)
		end
	end

	for i,v in ipairs(heads) do
		local check = {}

		if (found[v] == nil) then
			table.insert(check, v)
			table.insert(result, {})

			while (#check > 0) do
				local c = check[1]

				if (found[c] == nil) then
					found[c] = 1
					table.insert(result[#result], c)
					resultmap[c] = #result

					local count,cells = countsurrounds(c,cellmap,true)

					for a,b in ipairs(cells) do
						table.insert(check, b)
					end
				end

				table.remove(check, 1)
			end
		end
	end

	return result,resultmap
end

function getsnakes_line_branching(data)
	local cellmap = getmap(data)
	local result = {}
	local resultmap = {}

	local found = {}
	local heads = {}

	local origins = {}

	for i,v in pairs(cellmap) do
		local count = countsurrounds(i,cellmap,true)

		if (count >= 1) then
			table.insert(heads, i)
		end
	end

	for i,v in ipairs(heads) do
		local check = {}

		if (found[v] == nil) then
			table.insert(check, v)
			table.insert(result, {})

			while (#check > 0) do
				local c = check[1]

				local count,cells,dirs = countsurrounds(c,cellmap,true)

				if (found[c] == nil) or (count > 2) then
					found[c] = 1
					table.insert(result[#result], c)
					resultmap[c] = #result

					if (count <= 2) then
						for a,b in ipairs(cells) do
							table.insert(check, b)
						end
					else
						for a,b in ipairs(cells) do
							if (found[b] == nil) then
								table.insert(heads, b)
							end
						end
					end
				end

				table.remove(check, 1)
			end
		end
	end

	return result,resultmap
end

function ouroboros_check(snakes,snakemap)
	local cells = getcells({"M","F"})
	local cellmap = getmap(cells)

	local issues = {}

	for i,v in ipairs(cells) do
		if (snakemap[v.pos] ~= nil) then
			local x,y = getpos(v.pos)
			local ox,oy = getoffsetfromdir(x,y)

			if (ox == nil) or (oy == nil) then
				table.insert(issues, v.pos)
			end

			local count = countpointers(x,y)

			if (count ~= 1) then
				table.insert(issues, v.pos)
			end
		else
			table.insert(issues, v.pos)
		end
	end

	for i,v in ipairs(snakes) do
		local head = v[1]
		local tail = v[#v]

		local x1,y1 = getpos(head)
		local x2,y2 = getpos(tail)

		local ox1,oy1 = getoffsetfromdir(x1,y1)
		local ox2,oy2 = getoffsetfromdir(x2,y2)

		local fine = false

		if (ox1 ~= nil) and (oy1 ~= nil) and (ox2 ~= nil) and (oy2 ~= nil) then
			local tid1 = (x1 + ox1) + (y1 + oy1) * map.w
			local tid2 = (x2 + ox2) + (y2 + oy2) * map.w

			if (cellmap[head] ~= nil) and (cellmap[tail] ~= nil) then
				alert("Two heads")
				table.insert(issues, head)
				table.insert(issues, tail)
				table.insert(issues, tid1)
				table.insert(issues, tid2)
			elseif (cellmap[head] == nil) and (cellmap[tail] == nil) then
				alert("Two tails")
				table.insert(issues, head)
				table.insert(issues, tail)
				table.insert(issues, tid1)
				table.insert(issues, tid2)
			elseif (cellmap[head] ~= nil) and (cellmap[tid2] ~= nil) and inbounds(x2+ox2,y2+oy2) then
				fine = true
			elseif (cellmap[tail] ~= nil) and (cellmap[tid1] ~= nil) and inbounds(x1+ox1,y1+oy1) then
				fine = true
			end
		end

		if (fine == false) then
			alert("Invalid snake")
			table.insert(issues, head)
			table.insert(issues, tail)
		end
	end

	if (#issues > 0) then
		return false,issues
	end

	return true,{}
end

function getoffsetfromdir(x,y)
	local test = findcells(x,y,{"H","X"})
	local ox,oy

	if (#test > 0) then
		if (test[2] == "0") and (test[3] == "0") and (test[4] ~= "0") and (test[5] == "0") then
			ox = 1
			oy = 0
		elseif (test[2] == "0") and (test[3] == "0") and (test[4] == "0") and (test[5] ~= "0") then
			ox = 0
			oy = -1
		elseif (test[2] ~= "0") and (test[3] == "0") and (test[4] == "0") and (test[5] == "0") then
			ox = -1
			oy = 0
		elseif (test[2] == "0") and (test[3] ~= "0") and (test[4] == "0") and (test[5] == "0") then
			ox = 0
			oy = 1
		end
	end

	return ox,oy
end

function countsurrounds(tid,cellmap,line_)
	local count = 0
	local cells = {}
	local dirs = {}
	local x,y = getpos(tid)

	local line = line_ or false

	local count = 0
	local t1 = (x + 1) + (y) * map.w
	local t2 = (x) + (y - 1) * map.w
	local t3 = (x - 1) + (y) * map.w
	local t4 = (x) + (y + 1) * map.w

	if (cellmap[t1] ~= nil) and inbounds(x+1,y) then
		if (line == false) or connection_between_tid(tid,t1) then
			count = count + 1
			table.insert(cells, t1)
			table.insert(dirs, 0)
		end
	end
	if (cellmap[t2] ~= nil) and inbounds(x,y-1) then
		if (line == false) or connection_between_tid(tid,t2) then
			count = count + 1
			table.insert(cells, t2)
			table.insert(dirs, 1)
		end
	end
	if (cellmap[t3] ~= nil) and inbounds(x-1,y) then
		if (line == false) or connection_between_tid(tid,t3) then
			count = count + 1
			table.insert(cells, t3)
			table.insert(dirs, 2)
		end
	end
	if (cellmap[t4] ~= nil) and inbounds(x,y+1) then
		if (line == false) or connection_between_tid(tid,t4) then
			count = count + 1
			table.insert(cells, t4)
			table.insert(dirs, 3)
		end
	end

	return count,cells,dirs
end

function countcells_radial(x,y,targets,blockers,adjacency_,diagonal_)
	local dirs = {{1,0},{0,-1},{-1,0},{0,1}}
	local diags = {{1,1},{1,-1},{-1,-1},{-1,1}}

	local dircheck = dirs
	local result = 0

	local adjacency = adjacency_ or false
	local diagonal = diagonal_ or false

	if diagonal then
		dircheck = diags
	end

	for i,v in ipairs(dircheck) do
		local ox,oy = v[1],v[2]
		local cx,cy = x,y

		local doned = false

		while (doned == false) do
			cx = cx + ox
			cy = cy + oy

			if inbounds(cx,cy) then
				local t = findcells(cx,cy,targets)
				local b = findcells(cx,cy,blockers)

				if (#t > 0) then
					result = result + 1
				elseif adjacency then
					doned = true
				end

				if (#b > 0) then
					doned = true
				end
			else
				doned = true
			end
		end
	end

	return result
end

function u_turn(cells)
	local cellmap = {}

	for i,v in ipairs(cells) do
		cellmap[v.pos] = 1
	end

	for t1,v in pairs(cellmap) do
		local x,y = getpos(t1)

		local t2 = (x + 1) + (y) * map.w
		local t3 = (x + 1) + (y + 1) * map.w
		local t4 = (x) + (y + 1) * map.w

		if inbounds(x+1,y) and inbounds(x+1,y+1) and inbounds(x,y+1) then
			if (cellmap[t2] ~= nil) and (cellmap[t3] ~= nil) and (cellmap[t4] ~= nil) then
				local c1 = connection_between_tid(t1,t2)
				local c2 = connection_between_tid(t2,t3)
				local c3 = connection_between_tid(t3,t4)
				local c4 = connection_between_tid(t4,t1)

				local count = 0
				if c1 and c2 then
					count = count + 1
				end
				if c2 and c3 then
					count = count + 1
				end
				if c3 and c4 then
					count = count + 1
				end
				if c4 and c1 then
					count = count + 1
				end

				if (count > 1) then
					return true,{t1,t2,t3,t4}
				end
			end
		end
	end

	return false,{}
end

function findcells(x,y,types)
	local tileid = x + y * map.w
	local result = {}

	if (map.layout[tileid] ~= nil) and inbounds(x,y) then
		for i,v in pairs(map.layout[tileid]) do
			local d = {}
			for a,b in ipairs(v) do
				table.insert(d, b)
			end

			for a,b in ipairs(types) do
				if (v[1] == b) then
					return d
				end
			end
		end
	end

	return result
end

function getcells(types)
	local result = {}

	for x=0,map.w-1 do
		for y=0,map.h-1 do
			local tileid = x + y * map.w

			if (map.layout[tileid] ~= nil) then
				for i,v in pairs(map.layout[tileid]) do
					local yes = false

					for a,b in ipairs(types) do
						if (v[1] == b) then
							table.insert(result, {})
							yes = true
							break
						end
					end

					if yes then
						for a,b in ipairs(v) do
							table.insert(result[#result], b)
						end

						result[#result]["pos"] = tileid
						result[#result]["x"] = x
						result[#result]["y"] = y
					end
				end
			end
		end
	end

	return result
end

function getpos(tid)
	local x = math.floor(tid % map.w)
	local y = math.floor(tid / map.w)

	return x,y
end